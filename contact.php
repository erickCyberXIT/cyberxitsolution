<!--This is the main content-->
<div id="contact">
	<div class="headerBackground">
	   <h1>CONTACT US</h1>
	</div>
	<div id="contactContainer">
	<!--Two buttons for display to select what to do -->
	<table id="buttonTable">
		<tr>
			<td id="idButtonHeader" style="text-align: center;" colspan="3">
				please select one of the following options
			</td>
		</tr>
		<tr>
			<td>
				<input type="button" class="buttonStyle" id="btnContactInfo" value="Show Contact Information" style="width:205px" onclick="showContactInfo();" />&nbsp&nbsp&nbsp
				<input type="button" class="buttonStyle" id="btnLogQuery" value="Log a Query" style="width:106px" onclick="showLogQuery()"/>
				<input type="button" class="buttonStyle" id="btnReturn" value="Return to Selection" style="display: none; width:150px" onclick="returnToSelect()();" />
			<td>
		</tr>
	</table>
	<table id="buttonTableReturn">
		<tr>
			<td>
				<input type="button" class="buttonStyle" id="btnReturn" value="Return to Selection" style="width:150px" onclick="returnToSelect()();" />
			<td>
		</tr>
	</table>
	<br />
	<br />
	<table id="contactTable" style="border-collapse: initial;" cellpadding="10">
	
		<tr>
			<td colspan="3" style="text-align: center; font-size: 22px;">
					Contact Details 	
					<hr style="height: 2px; width:100%;"/>		
				</td>
		</tr>
		<tr>
			<td>
				Support: 
			</td>
			<td>
				support@cyberxit.co.za
			</td>
		</tr>
		<tr>
			<td>
				Accounts: 
			</td>
			<td>
				accounts@cyberxit.co.za
			</td>
		</tr>
		<tr>
			<td>
				Sales: 
			</td>
			<td>
				sales@cyberxit.co.za
			</td>
		</tr>
		<tr>
			<td>
				Contact Number:
			</td>
			<td>
				012 3334 555
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<img src="Images/cyber.jpg" width="270px" height="143px" style="position: relative; left:20%;" align="center"/>
			</td>
		</tr>
	</table>
	<table id="contactLogQueryTable"  style="border-collapse: initial;"cellpadding="10">
			<tr>
				<td colspan="3" style="text-align: center; font-size: 22px;">
					Log a Query 	
					<hr style="height: 2px; width:100%;"/>		
				</td>
			</tr>
			<tr>
			 
			 <td valign="top">
			 
			  <label for="first_name">First Name:</label>
			 
			 </td>
			 
			 <td valign="top">
			 
			  <input id="firstName" type="text" name="first_name" maxlength="50" size="30" required />
			 
			 </td>
			 
			</tr>
			 
			<tr>
			 
			 <td valign="top"">
			 
			  <label for="last_name">Last Name:</label>
			 
			 </td>
			 
			 <td valign="top">
			 
			  <input id="lastName" type="text" name="last_name" maxlength="50" size="30" required />
			 
			 </td>
			 
			</tr>
			 
			<tr>
			 
			 <td valign="top">
			 
			  <label for="email">Email Address:</label >
			 
			 </td>
			 
			 <td valign="top">
			 
			  <input id="emailId" type="text" name="email" maxlength="80" size="30" required />
			 
			 </td>
			 
			</tr>
			 
			<tr>
			 
			 <td valign="top">
			 
			  <label for="telephone">Contact Number:</label>
			 
			 </td>
			 
			 <td valign="top">
			 
			  <input id="tel" type="text" name="telephone" maxlength="30" size="30" />
			 
			 </td>
			 
			</tr>
			 
			<tr>
			 
			 <td valign="top">
			 
			  <label for="comments">Request/Enquiry:</label>
			 
			 </td>
			 
			 <td valign="top">
			 
			  <textarea id="comment" name="comments" maxlength="1000" cols="32" rows="6" style=" resize: none;" required></textarea>
			 
			 </td>
			 
			</tr>
			 
			<tr>
			 <td>
			 	
			 </td>
			 <td style="text-align:left">
			 
			  <input id="submitQuery" type="submit" class="buttonStyle" style="width: 160px" value="Submit Query">
			  <br />
			  <br />
			  <br />
			 </td>
			 
			</tr>
		</table>
	<div id="footerContent" style="position: relative; top: 210px;">
				    	<table>
				    			<tr>
				    				<td  style="position: relative; "><span>&#9786;</span>&nbsp;Support</td>
				    				<td  style="position: relative; "><span>&#x260E;</span>&nbsp;Contact Details</td>
				    				<td  style="padding-right: 20px; position: relative;"><a href="http://www.management.cyberxit.co.za/">GO TO MY WEBSITE</a></td>		
				    				<td  style="position: relative; "><span>&#9990;</span>&nbsp;Social Media</td>
				    				<td  style="position: relative; "><span>&#x3f;</span>&nbsp;About</td>
				    			</tr>
				    			<tr>
				    				<td>
				    					<ul style="font-size: 11pt; list-style-type: none;">
				    						<li>e-mail: support@cyberxit.co.za</li>
				    						<li>Log a ticket: <a href="http://www.management.cyberxit.co.za/">www.management.cyberxit.co.za</a></li>
				    					</ul>
				    				</td>	
				    				<td>
				    					<ul style="font-size: 11pt; list-style-type: none;">
				    						<li>e-mail: sales@cyberxit.co.za</li>
				    						<li>Address: 31st Avenue Gauteng Pretoria</li>
				    					</ul>
				    				</td>
				    				<td>
				    					
				    				</td>
				    				<td>
				    					 <ul style="font-size: 11pt; list-style-type: none;">
				    						<li><a href="https://www.facebook.com/cyberxit?ref=ts&fref=ts"><img id="facebook" src="Images/facebook.png" width="50" height="50"/></a></li>
				    					</ul>
				    				</td>	
				    				<td>
				    					 <ul style="font-size: 11pt; list-style-type: none;">
				    					 	<li>
				    					 		We CyberX IT specialize in the latest web software<br/>
				    					 		to suit your needs.
				    					 	</li>
				    					 </ul>
				    				</td>	
				    			</tr>
				    		</table>
				<div>Copyright © 2014. CyberX IT all rights reserved</div>
	 	</div>
	</div>
</div>