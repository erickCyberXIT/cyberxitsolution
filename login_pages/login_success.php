<?php
include_once '../clientportal/db_connect.php';
include_once '../clientportal/functions.php';

						 sec_session_start();
						 
						if (login_check($mysqli) == true) {
							$logged = 'in';
						} else {
							$logged = 'out';
						}
echo "<div id=\"panel\" style=\"display:block;	\">
		<div  class=\"content clearfix\" style=\"margin:0px auto; border:none; position: relative;\">";						
									
						if (isset($_GET['error'])) {
							echo '<p class="error">Error Logging In!</p>';
						}
						if (login_check($mysqli) == true) {
							//Create object to hold result
							$obj = new stdClass();
							$obj = getUserDetails(htmlentities($_SESSION['email_ses']), $mysqli); 
										
							echo "<div class=\"left\">
									<h2 id=\"companyName\">";
										echo $obj->companyName;
										echo "</h2>		
									<p id=\"companyInfo\" class=\"grey\">"; echo $obj->companyInfo; 
									echo "</p>	
								</div>";
							echo "<div class=\"left\" id=\"mainLogin\">
									<h2>Welcome ". htmlentities($_SESSION['username']) . "</h2>";
								echo '<p>Currently logged ' . $logged . ' as ' . htmlentities($_SESSION['username']) . '.</p>';
								echo '<p>Do you want to change user or logout? <a id="bt_logout" href="#">Log out</a>.</p>';
							echo "</div>";
						} else {						
							echo "<div class=\"left\">
				
								<h2>Client Login Portal</h2>		
								<p class=\"grey\">All registered CyberX IT clients can use this login form to login to their online client portal, this will allow you to
								access all your project content and client details. If you are not a client at CyberX IT you can still register to get all the latest
								updates on all new software release!</p>
								
							</div>
							<div class=\"left\" id=\"mainLogin\">
									<form id=\"login_form\" class=\"clearfix-login\">        
										<h2>Client Login</h2>
										<label class=\"grey\" for=\"name\">Email:</label>
										<input id=\"userEmail\"  class=\"field\" type=\"text\" name=\"email\" />
										<label class=\"grey\" for=\"pass\">Password:</label>
										<input id=\"userPass\" class=\"field\" type=\"password\" name=\"password\" id=\"password\"/>
										<label class=\"red\" style=\"display: none;\">Ivalid Username/Password. <br/>*Notice: Your account will be blocked on the third failed attempt!</label>
										<label><input name=\"rememberme\" id=\"rememberme\" type=\"checkbox\" checked=\"checked\" value=\"forever\" /> &nbsp;Remember me</label>
										<div class=\"clear\"></div>							
										<a class=\"lost-pwd\" href=\"#\">Lost your password?</a>						
									
										<input class=\"bt_login\" type=\"submit\" 
											   value=\"Login\" /> 
									</form>
						
							</div>";
							echo "<div id=\"mainRegister\" class=\"left right\"> 			
									<form id=\"register_form\" > 
										<h2>Client Registration</h2>						
										<label class=\"grey\" for=\"username\">Username:</label>
										<input class=\"field\" type='text' name='username' id='username' />
										<label class=\"grey\" for=\"email\">Email:</label> 
										<input class=\"field\" type=\"text\" name=\"email\" id=\"email\" />
										<label class=\"grey\" for=\"password\">Password:</label> 
										<input class=\"field\" type=\"password\" name=\"password\" id=\"password\"/>
										<label class=\"grey\" for=\"confirmpwd\">Confirm Password:</label> 
										<input class=\"field\" type=\"password\" name=\"confirmpwd\" id=\"confirmpwd\" />
										<label class=\"redRegister\" style=\"display: none;\"></label>
										<input class=\"bt_register\" type=\"submit\" value=\"Register\" /> 
									</form>
								</div>";
						}
						
echo "</div>
	</div>";


	echo "<div class=\"tab\"><ul  class=\"login\">
				<li class=\"left\">&nbsp;</li>
				<li>";
					if(login_check($mysqli) == true) {
						echo htmlentities($_SESSION['username']);
					} else {
						echo "Guest User"; 
					}
				echo "</li>
					<li class=\"sep\">|</li>
					<li id=\"toggle\">
						<a id=\"open\" class=\"open\"  href=\"#\" style=\"display: none;\">";
					if(login_check($mysqli) == true) {
						echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Logout";
					} else { 
						echo "Login | Register";
					} 
				echo "</a>
					  <a id=\"close\" style=\"display: block;\" class=\"close\" href=\"#\">Close Panel</a>			
					</li>
					<li class=\"right\">&nbsp;</li>
			</ul>";
		
			?>  