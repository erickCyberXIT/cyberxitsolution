<!DOCTYPE html>
<html>
<head>
<title>Coming Soon!</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script type="text/javascript" src="/Maintenance/social/js/jquery-1.10.2.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	var pageTitle = document.title; //HTML page title
	var pageUrl = location.href; //Location of the page

	
	//user hovers on the share button	
	$('#share-wrapper li').hover(function() {
		var hoverEl = $(this); //get element
		
		//browsers with width > 699 get button slide effect
		if($(window).width() > 699) { 
			if (hoverEl.hasClass('visible')){
				hoverEl.animate({"margin-left":"0px"}, "fast").removeClass('visible');
			} else {
				hoverEl.animate({"margin-left":"-117px"}, "fast").addClass('visible');
			}
		}
	});
		
	//user clicks on a share button
	$('.button-wrap').click(function(event) {
			var shareName = $(this).attr('class').split(' ')[0]; //get the first class name of clicked element
			
			switch (shareName) //switch to different links based on different social name
			{
				case 'facebook':
					var openLink = 'https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(pageUrl) + '&amp;title=' + encodeURIComponent(pageTitle);
					break;
				case 'twitter':
					var openLink = 'http://twitter.com/home?status=' + encodeURIComponent(pageTitle + ' ' + pageUrl);
					break;
				case 'digg':
					var openLink = 'http://www.digg.com/submit?phase=2&amp;url=' + encodeURIComponent(pageUrl) + '&amp;title=' + encodeURIComponent(pageTitle);
					break;
				case 'stumbleupon':
					var openLink = 'http://www.stumbleupon.com/submit?url=' + encodeURIComponent(pageUrl) + '&amp;title=' + encodeURIComponent(pageTitle);
					break;
				case 'delicious':
					var openLink = 'http://del.icio.us/post?url=' + encodeURIComponent(pageUrl) + '&amp;title=' + encodeURIComponent(pageTitle);
					break;
				case 'google':
					var openLink = 'https://plus.google.com/share?url=' + encodeURIComponent(pageUrl) + '&amp;title=' + encodeURIComponent(pageTitle);
					break;
				case 'email':
					var openLink = 'mailto:?subject=' + pageTitle + '&body=Found this useful link for you : ' + pageUrl;
					break;
				case 'Flickr':
					var openLink = 'https://secure.flickr.com/explore/=' + encodeURIComponent(pageUrl) + '&amp;title=' + encodeURIComponent(pageTitle);
					break;
			}
		
		//Parameters for the Popup window
		winWidth 	= 700;	
		winHeight	= 550;
		winLeft   	= ($(window).width()  - winWidth)  / 2,
		winTop    	= ($(window).height() - winHeight) / 2,	
		winOptions   = 'width='  + winWidth  + ',height=' + winHeight + ',top='    + winTop    + ',left='   + winLeft;
		
		//open Popup window and redirect user to share website.
		window.open(openLink,'Share This Link',winOptions);
		return false;
	});
});
</script>

<style type="text/css">
<!--
html { 
   background: url(/Maintenance/background/main.jpg) no-repeat center center fixed; 
   -webkit-background-size: cover;
   -moz-background-size: cover;
   -o-background-size: cover;
   background-size: cover;
}
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	font-family: Georgia, "Times New Roman", Times, serif;
	color: #333333;
	background: #FFFFFF;
	
/*Laptop/Tablet (1024px) */
@media only screen and (min-width: 481px) and (max-width: 1024px) and (orientation: landscape) {
}

/* Tablet Portrait (768px) */
@media only screen and (min-width: 321px) and (max-width: 1024px) and (orientation: portrait) {
}

/* Phone Landscape (480px) */
@media only screen and (min-width: 321px) and (max-width: 480px) and (orientation: landscape) {
}

/* Phone Portrait (320px) */
@media only screen and (max-width: 320px) {
}

/* iPad 3 & 4 Landscape */
@media only screen and (width: 481px) and (width: 1024px) and (orientation: landscape) {
}

/* iPad 3 & 4 Portrait */
@media only screen and (width: 481px) and (width: 1024px) and (orientation: portrait) {
}

/* iPhone 2G-3GS Landscape */
@media only screen and (width: 481px) and (width: 480px) and (orientation: landscape) {
}

/* iPhone 2G-3Gs Portrait */
@media only screen and (width: 481px) and (width: 480px) and (orientation: portrait) {
}

/* iPhone 4/4S Landscape */
@media only screen and (width: 569px) and (width: 480px) and (orientation: landscape) {
}

/* iPhone 4/4S Portrait */
@media only screen and (width: 569px) and (width: 480px) and (orientation: portrait) {
}

/* iPhone 5 Landscape */
@media only screen and (width: px) and (width: 568px) and (orientation: landscape) {
}

/* iPhone 5 Portrait */
@media only screen and (width: px) and (width: 568px) and (orientation: portrait) {
}
	
/* Smartphones (portrait and landscape) ----------- */
@media only screen 
and (min-device-width : 320px) 
and (max-device-width : 480px) {
/* Styles */
}

/* Smartphones (landscape) ----------- */
@media only screen 
and (min-width : 321px) {
/* Styles */
}

/* Smartphones (portrait) ----------- */
@media only screen 
and (max-width : 320px) {
/* Styles */
}

/* iPads (portrait and landscape) ----------- */
@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px) {
/* Styles */
}

/* iPads (landscape) ----------- */
@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px) 
and (orientation : landscape) {
/* Styles */
}

/* iPads (portrait) ----------- */
@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px) 
and (orientation : portrait) {
/* Styles */
}

/* Desktops and laptops ----------- */
@media only screen 
and (min-width : 1224px) {
/* Styles */
}

/* Large screens ----------- */
@media only screen 
and (min-width : 1824px) {
/* Styles */
}

/* iPhone 4 ----------- */
@media
only screen and (-webkit-min-device-pixel-ratio : 1.5),
only screen and (min-device-pixel-ratio : 1.5) {
/* Styles */
}
}
.wrapper {
	max-width: 800px;
	margin-right: auto;
	margin-left: auto;
	background: #F5F5F5;
	padding: 20px;
}


/* Share button */

/* outer wrapper */
#share-wrapper {
	margin-top: 180px;
	position:fixed;
	right: 0;
	z-index:1;
}

/* inner wrapper */
#share-wrapper ul.share-inner-wrp{
	list-style: none;
	margin: 0px;
	padding: 0px;
}

/* the list */
#share-wrapper li.button-wrap {
	background: #E4EFF0;
	padding: 0px 7px 0px 0px;
	display: block;
	width: 140px;
	margin: 0px -117px 20px 0px;
}

/* share link */
#share-wrapper li.button-wrap > a {
	padding-left: 35px;
	height: 32px;
	display: block;
	line-height: 32px;
	font-weight: bold;
	color: #444;
	text-decoration: none;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
}

/* background image for each link */
#share-wrapper .facebook > a{
	background: url(/Maintenance/social/buttons/facebook.jpg) no-repeat left;
}
#share-wrapper .twitter > a{
	background: url(/Maintenance/social/buttons/twitter.jpg) no-repeat left;
}
#share-wrapper .digg > a{
	background: url(/Maintenance/social/buttons/digg.jpg) no-repeat left;
}
#share-wrapper .stumbleupon > a{
	background: url(/Maintenance/social/buttons/stumbleupon.jpg) no-repeat left;
}
#share-wrapper .delicious > a{
	background: url(/Maintenance/social/buttons/delicious.jpg) no-repeat left;
}
#share-wrapper .google > a{
	background: url(/Maintenance/social/buttons/google.jpg) no-repeat left;
}
#share-wrapper .email > a{
	background: url(/Maintenance/social/buttons/email.jpg) no-repeat left;
}
#share-wrapper .flickr > a{
	background: url(/Maintenance/social/buttons/flickr.png) no-repeat left;
}

/* small screen */
@media all and (max-width: 699px) {
	#share-wrapper {
		bottom: 0;
		position: fixed;
		padding: 5px 5px 0px 5px;
		background: #EBEBEB;
		width: 100%;
		margin: 0px;
		-webkit-box-shadow: 0 -1px 4px rgba(0, 0, 0, 0.15);
		-moz-box-shadow: 0 -1px 4px rgba(0,0,0,0.15);
		-o-box-shadow: 0 -1px 4px rgba(0,0,0,0.15);
		box-shadow: 0 -1px 4px rgba(0, 0, 0, 0.15);
	}
	#share-wrapper ul.share-inner-wrp {
		list-style: none;
		margin: 0px auto;
		padding: 0px;
		text-align: center;
		overflow: auto;
	}
	#share-wrapper li.button-wrap {
		display: inline-block;
		width: 32px!important;
		margin: 0px;
		padding: 0px;
		margin-left:0px!important;
	}
	#share-wrapper li.button-wrap > a {
		height: 32px;
		display: inline-block;
		text-indent: -10000px;
		width: 32px;
		padding-right: 0;
		float: left;
}
/* Smartphones (portrait and landscape) ----------- */
@media only screen 
and (min-device-width : 320px) 
and (max-device-width : 480px) {
/* Styles */
}

/* Smartphones (landscape) ----------- */
@media only screen 
and (min-width : 321px) {
/* Styles */
}

/* Smartphones (portrait) ----------- */
@media only screen 
and (max-width : 320px) {
/* Styles */
}

/* iPads (portrait and landscape) ----------- */
@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px) {
/* Styles */
}

/* iPads (landscape) ----------- */
@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px) 
and (orientation : landscape) {
/* Styles */
}

/* iPads (portrait) ----------- */
@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px) 
and (orientation : portrait) {
/* Styles */
}

/* Desktops and laptops ----------- */
@media only screen 
and (min-width : 1224px) {
/* Styles */
}

/* Large screens ----------- */
@media only screen 
and (min-width : 1824px) {
/* Styles */
}

/* iPhone 4 ----------- */
@media
only screen and (-webkit-min-device-pixel-ratio : 1.5),
only screen and (min-device-pixel-ratio : 1.5) {
/* Styles */
}
}

!--->
</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script type="text/javascript" src="/Maintenance/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Maintenance/contact/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/Maintenance/contact/js/stickyfloat.js"></script>

<script type="text/javascript">

$( document ).ready(function() {
	$('#contact-wrapper').stickyfloat({ duration: 400, cssTransition: true, offsetY:100}); //stickyfloat contact form

	$("#contact-btn").click(function() { //smoothly slide open/close form
		var floatbox = $("#floating-contact-wrap");
		if (floatbox.hasClass('visiable')){
			floatbox.animate({"right":"-340px"}, "fast").removeClass('visiable');
		}else{
			floatbox.animate({"right":"0px"}, "fast").addClass('visiable');
}
	});
	
	$("#message-send-btn").click(function() { //try sending email
	
		//validate the form
		 var validator 		= $( "#floating-contact").validate();
		 var isValid 		= validator.form();
		 
			if(isValid){ //is everyting valid? proceed

				//collect values from input fields
				var user_name       = $('.floating-contact-inner input[name=name]').val(); 
				var user_email      = $('.floating-contact-inner input[name=email]').val();
				var user_message    = $('.floating-contact-inner textarea[name=message]').val();
				
				//prepare ajax data
				post_data = {'s_name':user_name, 's_email':user_email, 's_message':user_message};
				
				$(this).hide(); //hide submit button
				$('#ajax-loading-image').show(); //show loading image
				
				$.post('/Maintenance/contact/ajax_response.php', post_data, function(data){  
						validator.resetForm();
						//load success massage in #result div element,  
						$("#result").hide().html('<div class="success">'+data+'</div>').slideDown();
						$('#floating-contact').hide(); //show submit button
						
					}).fail(function(err) {  //load any error data
					
						$("#result").hide().html('<div class="error">'+err.statusText+'</div>').slideDown();
						$("#message-send-btn").show(); //show submit button
						$('#ajax-loading-image').hide(); //hide loading image

				});
			
			}else{
				$("#message-send-btn").show(); //show submit button
				$('#ajax-loading-image').hide(); //hide loading image
			}
	});
	
});
</script>
<style type="text/css">
#contact-wrapper{
    position: absolute;
    right: 0px;
    width: 400px;
    height: 150px;
    overflow:hidden;
    z-index:9999;
    top:0px;
}
#contact-wrapper .floating-contact-inner {
    position: absolute;
    width: 320px;
    background: #3c424a url(/Maintenance/contact/image/denim.png) repeat;
    padding: 15px 10px 500px 10px;
    font: 12px Arial, Helvetica, sans-serif;
    text-shadow: 1px 1px 1px #3C3C3C;
    color: #FFF;
    border-radius: 10px 0px 0px 10px;
    right: -340px;
}
#contact-wrapper #contact-btn {
    background: url(/Maintenance/contact/image/contact-btn.png) no-repeat;
    height: 71px;
    width: 22px;
    float: left;
    cursor: pointer;
    margin: 0px 10px 0px -32px;
    
} 
#contact-wrapper #contact-btn:hover {
    background: url(/Maintenance/contact/image/contact-btn.png) no-repeat -22px 0px;
}
</style>
</head>

<body>
<div id="share-wrapper">
    <ul class="share-inner-wrp">
        <!-- Facebook -->
        <li class="facebook button-wrap"><a href="#">Facebook</a></li>
        
        <!-- Twitter -->
        <li class="twitter button-wrap"><a href="#">Tweet</a></li>
        
         <!-- Digg -->
        <li class="digg button-wrap"><a href="#">Digg it</a></li>
        
        <!-- Stumbleupon -->
        <li class="stumbleupon button-wrap"><a href="#">Stumbleupon</a></li>
      
         <!-- Delicious -->
        <li class="delicious button-wrap"><a href="#">Delicious</a></li>
        
        <!-- Google -->
        <li class="google button-wrap"><a href="#">Plus Share</a></li>
        
        <!-- Email -->
        <li class="email button-wrap"><a href="#">Email</a></li>
        
        <!-- Flickr -->
        <li class="flickr button-wrap"><a href="#">Flickr</a></li>
</ul>
</div>
<div id="contact-wrapper">
<div class="floating-contact-inner" id="floating-contact-wrap">
<div id="contact-btn">&nbsp;</div>
<div id="result"></div>
<form action="" method="post" id="floating-contact" >
    <label>
        <span></span>
        <input id="name" type="text" name="name" placeholder="Your Full Name"  required />
    </label>
    <label>
        <span></span>
        <input id="email" type="email" name="email" placeholder="Valid Email Address"  email required />
    </label>
    <label>
        <span></span>
        <textarea id="message" name="message" placeholder="Your Message to Us" required></textarea>
    </label> 
     <label>
        <span>&nbsp;</span> 
        <input type="button" class="button" id="message-send-btn" value="Send" />
        <img id="ajax-loading-image" style="display:none" src="/Maintenance/contact/image/ajax-loader.gif" border="0" />
    </label>    
</form>
</div>
</div>
<!-- end floating contact form markup -->
</body>
</html>