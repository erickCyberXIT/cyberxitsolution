<?php
include_once 'db_connect.php';
include_once 'functions.php';
 
sec_session_start(); // Our custom secure way of starting a PHP session.
 
if (isset($_POST['email'], $_POST['p'])) {
    $email = $_POST['email'];
    $password = $_POST['p']; // The hashed password.
	
	//Create array of objects that will be returned as json  
	$response = array();
		
	if (checkBlocked($email, $password, $mysqli) == true) {// Login failed     
		
		$response['status'] = "blocked";
		//Return the encoded json string
		echo json_encode($response);	
		return;
		
	} else if (login($email, $password, $mysqli) == true) {
		
		//get the user details 
		//Create new obj 
		$obj = new stdClass();
		$obj = getUserDetails($email, $mysqli);
		
		//Assign the array objects 
		$response['email'] = $email;
		$response['status'] = "success";
		$response['companyName'] = $obj->companyName;
		$response['companyInfo'] = $obj->companyInfo;
		
		//Return the json encoded resoponse object
		echo json_encode($response);
		return;
		
    } else {
		
		//Assign the array objects 
		$response['status'] = "failed";
		
		//Return the json encoded resoponse object
		echo json_encode($response);
		return;

	}
} else {
    // The correct POST variables were not sent to this page. 
    echo 'Invalid Request';
}