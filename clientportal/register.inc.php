<?php
include_once 'db_connect.php';
include_once 'psl-config.php';
 
$error_msg = "";
//Create global response object
$response = Array();

if (isset($_POST['username'], $_POST['email'], $_POST['p'])) {
    // Sanitize and validate the data passed in
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
    $email = filter_var($email, FILTER_VALIDATE_EMAIL);
		
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$response['status'] = "Invalid Email Address!";
		//Return the object
		echo json_encode($response);
		return false;
    }
 
    $password = filter_input(INPUT_POST, 'p', FILTER_SANITIZE_STRING);
    if (strlen($password) != 128) {
        // The hashed pwd should be 128 characters long.
        // If it's not, something really odd has happened
        $response['status'] = "Invalid password configuration!";
		//Return the object
		echo json_encode($response);
		return false;
    }
 
    // Username validity and password validity have been checked client side.
    // This should should be adequate as nobody gains any advantage from
    // breaking these rules.
    //
 
    $prep_stmt = "SELECT id FROM table_users WHERE email = ? LIMIT 1";
    $stmt = $mysqli->prepare($prep_stmt);
 
   // check existing email  
    if ($stmt) {
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $stmt->store_result();
 
        if ($stmt->num_rows == 1) {
            // A user with this email address already exists
			$stmt->close();
			$response['status'] = "Already a user registered with this email!";
				//Return the response 
				echo json_encode($response);
				return false;
        }
                $stmt->close();
    } else {
          
                $stmt->close();
			  $response['status'] = "Database Error, check syntax or for updates!";
			//return response
			echo json_encode($response);
			return false;
    }
 
    // check existing username
    $prep_stmt = "SELECT id FROM table_users WHERE username = ? LIMIT 1";
    $stmt = $mysqli->prepare($prep_stmt);
 
    if ($stmt) {
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $stmt->store_result();
 
                if ($stmt->num_rows == 1) {
					$stmt->close();
                        // A user with this username already exists
                        $response['status'] = "Already a user registered with this username!";
                        
						//return response
						echo json_encode($response);
						return false;
                }
                $stmt->close();
        } else {
			 $stmt->close();
                 $response['status'] = "Database Error, check syntax or for updates!";
               
				//return response
			echo json_encode($response);
			return false;
        }
 
    // TODO: 
    // We'll also have to account for the situation where the user doesn't have
    // rights to do registration, by checking what type of user is attempting to
    // perform the operation.
 
    if (empty($response)) {
        // Create a random salt
        //$random_salt = hash('sha512', uniqid(openssl_random_pseudo_bytes(16), TRUE)); // Did not work
        $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
 
        // Create salted password 
        $password = hash('sha512', $password . $random_salt);
 
        // Insert the new user into the database 
        if ($insert_stmt = $mysqli->prepare("INSERT INTO table_users (username, email, password, salt) VALUES (?, ?, ?, ?)")) {
            $insert_stmt->bind_param('ssss', $username, $email, $password, $random_salt);
            // Execute the prepared query.
            if (! $insert_stmt->execute()) {
               $response['status'] = "Error inserting user into database, please review insert query on registration!";
			   //return response
				echo json_encode($response);
				return false;
            }
        }
		
         $response['status'] = "success";
		 //return respons
		echo json_encode($response); 
		return true;
    }
}