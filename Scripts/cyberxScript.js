//THIS IS CYBER X IT JAVASCRIPT//
// 			ANIMATION 		   //
////////////////////////////////

//STATIC FUNCTIONS//
//---------------//
$(document).ready(function(){
    $("#myWebControlFooter").hover(function(){
	    $("#footerImageID").attr("src", "Styles/Images/footerImage_Hover.png");
		}, function() { 
 	    $("#footerImageID").attr("src", "Styles/Images/footerImage.png");
	});
});


		//FUNCTION TO DETECT THE BROWSER VERSION//
		//--------------------------------------//
		function get_browser_info(){
		    var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || []; 
		    if(/trident/i.test(M[1])){
		        tem=/\brv[ :]+(\d+)/g.exec(ua) || []; 
		        return {name:'IE ',version:(tem[1]||'')};
		        }   
		    if(M[1]==='Chrome'){
		        tem=ua.match(/\bOPR\/(\d+)/)
		        if(tem!=null)   {return {name:'Opera', version:tem[1]};}
		        }   
		    M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
		    if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
		    return {
		      name: M[0],
		      version: M[1]
		    };
		 }
		 
		 var browser = get_browser_info();
		// browser.name = 'Chrome'
		// browser.version = '40'
		//to test it alert(browser.name);
 		//------------------------------------////
 		
 		//ANIMATION RENDER FUNCTION FOR ENTIRE PAGE//
 		//----------------------------------------//
 		//BROWSER DETECTION// 
		
					

			
			if (browser.name == "IE " || browser.name == "MSIE") {					 
					 
							
		    			 $("#logoImage").fadeTo(800, 1);
						//$("#logoImage").animate({left: "+=500"}, 2000); -- make go right
						$("#logoImage").animate({left: "+=36%"}, 1000);  // -- make go left
						$("#logoImage").animate({left: "-=5%"}, 450);  // -- make go left
						$("#logoImage").animate({left: "+=4%"}, 250);  // -- make go left
						$("#logoImage").animate({left: "-=1.5%"}, 150);  // -- make go left
						$("#logoImage").animate({left: "+=0.5%"}, 80);  // -- make go left
						$("#logoImage").animate({left: "-=0.5%"}, 40);  // -- make go left
						setTimeout(function() {$(".menu-btn").fadeTo(2000, 1)}, 3000);	
						setTimeout(function() {$("#footerContent").fadeTo(2000, 1)}, 3000);		
						setTimeout(function() {$("#da-slider").fadeTo(2800, 1)}, 4000);
						setTimeout(function() {$("#arrowleft").fadeTo(2800, 1)}, 4000);
						setTimeout(function() {$("#arrowright").fadeTo(2800, 1)}, 4000);
						setTimeout(function() {$("#buttonNav").fadeTo(2800, 1)}, 4000);									
			
							
				} else if(browser.name == "Chrome"){
					
					
		    			 $("#logoImage").fadeTo(800, 1);
						//$("#logoImage").animate({left: "+=500"}, 2000); -- make go right
						$("#logoImage").animate({left: "+=36%"}, 1000);  // -- make go left
						$("#logoImage").animate({left: "-=5%"}, 450);  // -- make go left
						$("#logoImage").animate({left: "+=4%"}, 250);  // -- make go left
						$("#logoImage").animate({left: "-=1.5%"}, 150);  // -- make go left
						$("#logoImage").animate({left: "+=0.5%"}, 80);  // -- make go left
						$("#logoImage").animate({left: "-=0.5%"}, 40);  // -- make go left
						setTimeout(function() {$(".menu-btn").fadeTo(2000, 1)}, 3000);	
						setTimeout(function() {$("#footerContent").fadeTo(2000, 1)}, 3000);		
						setTimeout(function() {$("#da-slider").fadeTo(2800, 1)}, 4000);
						setTimeout(function() {$("#arrowleft").fadeTo(2800, 1)}, 4000);
						setTimeout(function() {$("#arrowright").fadeTo(2800, 1)}, 4000);
						setTimeout(function() {$("#buttonNav").fadeTo(2800, 1)}, 4000);		
		 				
				} else { //Firefox
					$("#logoImage").fadeTo(800, 1);
											//$("#logoImage").animate({left: "+=500"}, 2000); -- make go right
						$("#logoImage").animate({left: "+=36%"}, 1000);  // -- make go left
						$("#logoImage").animate({left: "-=5%"}, 450);  // -- make go left
						$("#logoImage").animate({left: "+=4%"}, 250);  // -- make go left
						$("#logoImage").animate({left: "-=1.5%"}, 150);  // -- make go left
						$("#logoImage").animate({left: "+=0.5%"}, 80);  // -- make go left
						$("#logoImage").animate({left: "-=0.5%"}, 40);  // -- make go left
						setTimeout(function() {$(".menu-btn").fadeTo(2000, 1)}, 3000);	
						setTimeout(function() {$("#footerContent").fadeTo(2000, 1)}, 3000);		
						setTimeout(function() {$("#da-slider").fadeTo(2800, 1)}, 4000);
						setTimeout(function() {$("#arrowleft").fadeTo(2800, 1)}, 4000);
						setTimeout(function() {$("#arrowright").fadeTo(2800, 1)}, 4000);
						setTimeout(function() {$("#buttonNav").fadeTo(2800, 1)}, 4000);
		
		
				}
			
		//----------------------------------------//
		
		//Dynamic styling
		
		//Menu (Navigation)
		$("#idSpecials a, #idSpecials ul li a").hover(
			function(){
				$(".arrow").html("&#x25BC"); // you can use every class name you want of course
			},
			function(){
			$(".arrow").html("&#x25BA");
			}
		);
		
		//Products (Expander)
		function doSlide(param) {
			
			//--------------------------------WEB SOFTWARE------------------------------------//
			if (param === 'webSoftware') {
				
				var status = $("#webSoftware").attr('class');
				
				if(status === 'active'){
						//Set tab clour 
						$("#webSoftware").removeAttr("style")
						
						//Animate
						$("#websoftwareDiv").slideUp(700);
						//Remove current class
						$("#webSoftware").removeClass();
						//Set class inactive
						$("#webSoftware").addClass("inactive");
						//Set class as inactive
						$(".tableArrow-web").html("&#x25BA");
			
				} else if ($("#hosting").attr('class') === 'active') {
					
						//Set tab clour 
						$("#hosting").removeAttr("style")
						//Slide up products
						$("#hostingDiv").slideUp(700);
						//Remove current class
						$("#hosting").removeClass();
						//Set class inactive
						$("#hosting").addClass("inactive");
						//Set class as inactive
						$(".tableArrow-host").html("&#x25BA");
					
						//Set tab colour
						$("#webSoftware").css({ "background-color":"black", "opacity":"0.6"});
						//Slide down
						$("#websoftwareDiv").slideDown(700);
						//Remove current class
						$("#webSoftware").removeClass();
						//Set class active
						$("#webSoftware").addClass("active");
						//Set cLass active
						$(".tableArrow-web").html("&#x25BC");
						
				} else if ($("#deskSoftware").attr('class') === 'active') {
						//Set tab clour 
						$("#deskSoftware").removeAttr("style")
						//Slide up products
						$("#deskSoftwareDiv").slideUp(700);
						//Remove current class
						$("#deskSoftware").removeClass();
						//Set class inactive
						$("#deskSoftware").addClass("inactive");
						//Set class as inactive
						$(".tableArrow-desk").html("&#x25BA");
						
						//Set tab colour
						$("#webSoftware").css({ "background-color":"black", "opacity":"0.6"});
						//Animate
						$("#websoftwareDiv").slideDown(700);
						//Remove current class
						$("#webSoftware").removeClass();
						//Set class inactive
						$("#webSoftware").addClass("active");
						
						//Set ckass active
						$(".tableArrow-web").html("&#x25BC");
						
				} else if ($("#maintenance").attr('class') === 'active') {
						//Set tab clour 
						$("#maintenance").removeAttr("style")
						//Slide up products
						$("#maintenanceDiv").slideUp(700);
						//Remove current class
						$("#maintenance").removeClass();
						//Set class inactive
						$("#maintenance").addClass("inactive");
						//Set class as inactive
						$(".tableArrow-web").html("&#x25BA");
						
						//Set tab colour
						$("#webSoftware").css({ "background-color":"black", "opacity":"0.6"});
						//Animate
						$("#websoftwareDiv").slideDown(700);
						//Remove current class
						$("#webSoftware").removeClass();
						//Set class inactive
						$("#webSoftware").addClass("active");
						
						//Set ckass active
						$(".tableArrow-web").html("&#x25BC");
						
				} else if (status === 'inactive'){
						//Set tab colour
						$("#webSoftware").css({ "background-color":"black", "opacity":"0.6"});

						//Animate
						$("#websoftwareDiv").slideDown(700);
						//Remove current class
						$("#webSoftware").removeClass();
						//Set class inactive
						$("#webSoftware").addClass("active");
						
						//Set ckass active
						$(".tableArrow-web").html("&#x25BC");
						
				}	
				
				//Set focus 
				$("#webSoftware").focus();
			//--------------------------------HOSTING------------------------------------------//	
			} else if (param === 'hosting') {
				
				var status = $("#hosting").attr('class');
				
				if(status === 'active'){
						//Set tab clour 
						$("#hosting").removeAttr("style")
						//Animate
						$("#hostingDiv").slideUp(700);
						//Remove current class
						$("#hosting").removeClass();
						//Set class inactive
						$("#hosting").addClass("inactive");
						
						//Set class as inactive
						$(".tableArrow-host").html("&#x25BA");
			
				} else if ($("#webSoftware").attr('class') === 'active') {
						//Set tab clour 
						$("#webSoftware").removeAttr("style")
						//Slide up products
						$("#websoftwareDiv").slideUp(700);
						//Remove current class
						$("#webSoftware").removeClass();
						//Set class inactive
						$("#webSoftware").addClass("inactive");
						//Set class as inactive
						$(".tableArrow-web").html("&#x25BA");
						
						//Set tab clour 
						$("#hosting").css({ "background-color":"black", "opacity":"0.6"});
						//Animate
						$("#hostingDiv").slideDown(700);
						//Remove current class
						$("#hosting").removeClass();
						//Set class inactive
						$("#hosting").addClass("active");
						//Set class as inactive
						$(".tableArrow-host").html("&#x25BC");
						
				} else if ($("#deskSoftware").attr('class') === 'active') {
						
						//Set tab clour 
						$("#deskSoftware").removeAttr("style")
						//Slide up products
						$("#deskSoftwareDiv").slideUp(700);
						//Remove current class
						$("#deskSoftware").removeClass();
						//Set class inactive
						$("#deskSoftware").addClass("inactive");
						//Set class as inactive
						$(".tableArrow-desk").html("&#x25BA");
						
						//Set tab clour 
						$("#hosting").css({ "background-color":"black", "opacity":"0.6"});
						//Animate
						$("#hostingDiv").slideDown(700);
						//Remove current class
						$("#hosting").removeClass();
						//Set class inactive
						$("#hosting").addClass("active");
						//Set class as inactive
						$(".tableArrow-host").html("&#x25BC");
				
				} else if ($("#maintenance").attr('class') === 'active') {
						//Set tab clour 
						$("#maintenance").removeAttr("style")
						//Slide up products
						$("#maintenanceDiv").slideUp(700);
						//Remove current class
						$("#maintenance").removeClass();
						//Set class inactive
						$("#maintenance").addClass("inactive");
						//Set class as inactive
						$(".tableArrow-web").html("&#x25BA");
						
						//Set tab clour 
						$("#hosting").css({ "background-color":"black", "opacity":"0.6"});
						//Animate
						$("#hostingDiv").slideDown(700);
						//Remove current class
						$("#hosting").removeClass();
						//Set class inactive
						$("#hosting").addClass("active");
						//Set class as inactive
						$(".tableArrow-host").html("&#x25BC");
						
				} else if (status === 'inactive'){
						//Set tab clour 
						$("#hosting").css({ "background-color":"black", "opacity":"0.6"});
						//Animate
						$("#hostingDiv").slideDown(700);
						//Remove current class
						$("#hosting").removeClass();
						//Set class inactive
						$("#hosting").addClass("active");
						//Set class as inactive
						$(".tableArrow-host").html("&#x25BC");
				} 	
				//Set focus 
				$("#hosting").focus();
			//--------------------------------DESK SOFTWARE------------------------------------------//	
			} else if (param === 'deskSoftware') {
				
				var status = $("#deskSoftware").attr('class');
				
				if(status === 'active'){
						//Set tab clour 
						$("#deskSoftware").removeAttr("style")
						//Animate
						$("#deskSoftwareDiv").slideUp(700);
						//Remove current class
						$("#deskSoftware").removeClass();
						//Set class inactive
						$("#deskSoftware").addClass("inactive");
						
						//Set class as inactive
						$(".tableArrow-desk").html("&#x25BA");
			
				} else if ($("#hosting").attr('class') === 'active') {
						
						//Set tab clour 
						$("#hosting").removeAttr("style")
						//Slide up products
						$("#hostingDiv").slideUp(700);
						//Remove current class
						$("#hosting").removeClass();
						//Set class inactive
						$("#hosting").addClass("inactive");
						//Set class as inactive
						$(".tableArrow-host").html("&#x25BA");
						
						//Set tab clour 
						$("#deskSoftware").css({ "background-color":"black", "opacity":"0.6"});
						//Animate
						$("#deskSoftwareDiv").slideDown(700);
						//Remove current class
						$("#deskSoftware").removeClass();
						//Set class inactive
						$("#deskSoftware").addClass("active");
						//Set class as active
						$(".tableArrow-desk").html("&#x25BC");
						
				} else if ($("#webSoftware").attr('class') === 'active') {
						//Set tab clour 
						$("#webSoftware").removeAttr("style")
						//Slide up products
						$("#websoftwareDiv").slideUp(700);
						//Remove current class
						$("#webSoftware").removeClass();
						//Set class inactive
						$("#webSoftware").addClass("inactive");
						//Set class as inactive
						$(".tableArrow-web").html("&#x25BA");
						
						//Set tab clour 
						$("#deskSoftware").css({ "background-color":"black", "opacity":"0.6"});
						//Animate
						$("#deskSoftwareDiv").slideDown(700);
						//Remove current class
						$("#deskSoftware").removeClass();
						//Set class inactive
						$("#deskSoftware").addClass("active");
						//Set class as active
						$(".tableArrow-desk").html("&#x25BC");
			
				
				} else if ($("#maintenance").attr('class') === 'active') {
						//Set tab clour 
						$("#maintenance").removeAttr("style")
						//Slide up products
						$("#maintenanceDiv").slideUp(700);
						//Remove current class
						$("#maintenance").removeClass();
						//Set class inactive
						$("#maintenance").addClass("inactive");
						//Set class as inactive
						$(".tableArrow-web").html("&#x25BA");
						
						//Set tab clour 
						$("#deskSoftware").css({ "background-color":"black", "opacity":"0.6"});
						//Animate
						$("#deskSoftwareDiv").slideDown(700);
						//Remove current class
						$("#deskSoftware").removeClass();
						//Set class inactive
						$("#deskSoftware").addClass("active");
						//Set class as active
						$(".tableArrow-desk").html("&#x25BC");
					
				} else if (status === 'inactive'){
						//Set tab clour 
						$("#deskSoftware").css({ "background-color":"black", "opacity":"0.6"});
						//Animate
						$("#deskSoftwareDiv").slideDown(700);
						//Remove current class
						$("#deskSoftware").removeClass();
						//Set class inactive
						$("#deskSoftware").addClass("active");
						//Set class as active
						$(".tableArrow-desk").html("&#x25BC");
						
				} 					
				//Set focus 
				$("#deskSoftware").focus();
			//----------------------------------MAINTENANCE-----------------------------//
			} else if (param == 'maintenance'){
				var status = $("#maintenance").attr('class');
				
				if(status === 'active'){
						//Set tab clour 
						$("#maintenance").removeAttr("style")
						//Animate
						$("#maintenanceDiv").slideUp(700);
						//Remove current class
						$("#maintenance").removeClass();
						//Set class inactive
						$("#maintenance").addClass("inactive");
						//Set class as inactive
						$(".tableArrow-maint").html("&#x25BA");
			
				} else if ($("#hosting").attr('class') === 'active') {
						//Set tab clour 
						$("#hosting").removeAttr("style")
						//Slide up products
						$("#hostingDiv").slideUp(700);
						//Remove current class
						$("#hosting").removeClass();
						//Set class inactive
						$("#hosting").addClass("inactive");
						//Set class as inactive
						$(".tableArrow-host").html("&#x25BA");
						
						//Set tab clour 
						$("#maintenance").css({ "background-color":"black", "opacity":"0.6"});
						//Animate
						$("#maintenanceDiv").slideDown(700);
						//Remove current class
						$("#maintenance").removeClass();
						//Set class inactive
						$("#maintenance").addClass("active");
						
						//Set ckass active
						$(".tableArrow-maint").html("&#x25BA");
						
				} else if ($("#deskSoftware").attr('class') === 'active') {
						
						//Set tab clour 
						$("#deskSoftware").removeAttr("style")
						//Slide up products
						$("#deskSoftwareDiv").slideUp(700);
						//Remove current class
						$("#deskSoftware").removeClass();
						//Set class inactive
						$("#deskSoftware").addClass("inactive");
						//Set class as inactive
						$(".tableArrow-desk").html("&#x25BA");
						
						//Set tab clour 
						$("#maintenance").css({ "background-color":"black", "opacity":"0.6"});
						//Animate
						$("#maintenanceDiv").slideDown(700);
						//Remove current class
						$("#maintenance").removeClass();
						//Set class inactive
						$("#maintenance").addClass("active");
						
						//Set ckass active
						$(".tableArrow-maint").html("&#x25BA");
						
				} else if ($("#webSoftware").attr('class') === 'active') {
						//Set tab clour 
						$("#webSoftware").removeAttr("style")
						//Slide up products
						$("#websoftwareDiv").slideUp(700);
						//Remove current class
						$("#webSoftware").removeClass();
						//Set class inactive
						$("#webSoftware").addClass("inactive");
						//Set class as inactive
						$(".tableArrow-web").html("&#x25BA");
						
						//Set tab clour 
						$("#maintenance").css({ "background-color":"black", "opacity":"0.6"});
						//Animate
						$("#maintenanceDiv").slideDown(700);
						//Remove current class
						$("#maintenance").removeClass();
						//Set class inactive
						$("#maintenance").addClass("active");
						
						//Set ckass active
						$(".tableArrow-maint").html("&#x25BA");
						
				} else if (status === 'inactive'){
						//Set tab clour 
						$("#maintenance").css({ "background-color":"black", "opacity":"0.6"});
						//Animate
						$("#maintenanceDiv").slideDown(700);
						//Remove current class
						$("#maintenance").removeClass();
						//Set class inactive
						$("#maintenance").addClass("active");
						
						//Set ckass active
						$(".tableArrow-maint").html("&#x25BA");
				}	
				
			}
			$('html, body').animate({
				scrollTop: $(".headerBackground").offset().top
			}, 800);
		}
		
		//Button Functions on Contact Form 
		function showContactInfo() {
			//Hide the following
			$("#buttonTable").fadeOut(600);
			
			//Show the following
			setTimeout(function() {
				
				$("#buttonTableReturn").fadeIn(600); 
				$("#contactTable").fadeIn(600);
				
			}, 600);
			
		}
		
		function returnToSelect() {
			
			//Hide the following
			$("#buttonTableReturn").fadeOut(600);
			$("#contactLogQueryTable").fadeOut(600);
			$("#contactTable").fadeOut(600);
			
			//Show the buttons
			setTimeout(function() {
				
				$("#buttonTable").fadeIn(600);
				$("#btnLogQuery").fadeIn(600);
				
			}, 600);
		}
		
		function showLogQuery() {
		
			//Hide the following
			$("#buttonTable").fadeOut(600);			
			
			//Show the following
			setTimeout(function() {
				
				$("#buttonTableReturn").fadeIn(600); 
				$("#contactLogQueryTable").fadeIn(600);
				
			}, 600);
		}
				
			function toggleItems() {
				$("#cssmenu").animate({
					width: 'toggle'
				});
			}
			

			
			
			function toggleMenu() {
				var txtClosed = "Menu<span>&nbsp;&nbsp;&#x25BA;</span>";
				var txtOpen = "Menu<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#x25C4;</span>";
				var toggleWidthNav = $("#buttonNav").width() == 200 ? "85px" : "200px";
				
				$("#buttonNav a").css(
					'padding-right', '0px'
				);

				//Set the text
				if ($("#buttonNav").width() == 200) {
					$("#buttonNav a").html(txtClosed);		
				} else {
					$("#buttonNav a").html(txtOpen);
				}
							
				$("#buttonNav").animate({
					width: toggleWidthNav
				});
				
				$("#buttonNav ul").animate({
					width: toggleWidthNav
				});
				
				$("#buttonNav ul").animate({
					'padding-right' : '20px',
				});		
			}
			
		function hoverCssMenu() {
			
			$("#cssmenu").hover(function(event){
					//do nothing
				}, function(){
					toggleItems();
					toggleMenu();
					 event.stopPropagation();
			});
			
		}
		
		$(document).ready(function(){
			hoverCssMenu();	
		})
		
		//nav menu item
		function toggleNav() {
			toggleItems();
			toggleMenu();
			
		} 
		
		$("#buttonNav a").click(function(){
			toggleNav();
		});
		
		//bind click 
		$("#cssmenu a").click(function(){
			$("#cssmenu").unbind('hover');
			toggleNav();	
			$("#cssmenu").bind('hover', function(){
				hoverCssMenu();
			});
		})

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		