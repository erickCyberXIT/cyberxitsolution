$(document).ready(function() {
	function bindTogle(){
		// Expand Panel
		$("#open").bind("click", function(){
			$("div#panel").slideDown("slow");
		
		});	
		
		// Collapse Panel
		$("#close").bind("click", function(){
			$("div#panel").slideUp("slow");	
		});		
		
		// Switch buttons from "Log In | Register" to "Close Panel" on click
		$("#toggle a").bind("click", function () {
			$("#toggle a").toggle();
		});		
	}
	
	//bind event handllers for togel pannel
	bindTogle();
});