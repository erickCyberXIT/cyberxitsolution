//Validate contact us form 
//Email regular expression function
function isValidEmailAddress(emailAddress) {
var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
return pattern.test(emailAddress);
};

function validateForm() {

		
	//Declare test variables 
	var firstName = $("#firstName");
	var lastName = $("#lastName");
	var email = $("#email");
	var comment = $("#comment");

	$("#submitQuery").click(function() {
		if(firstName == "") {
			alert("Please enter your name!");
			firstName.focus();
			return false;
		} else if (lastName == "") {
			alert("Please enter your last name!");
			lastName.focus();
			return false;
		} else if (email == ""){
			alert("Please enter your email!");
			email.focus();
			return false;
		} else if (!isValidEmailAddress(email.val())) {
			alert("Please enter a valid email!");
			email.focus();
			return false;
		} else if (comment == ""){
			alert("Please enter your query reason");
			comment.focus();
			return false;
		}
	});
}

//global function to check for empty input
function checkEmpty(element) {
	if ($(element).val().replace(/ /g,'') == '') 
		return true;
	else 
		return false;
}

//global function to check for key up to hide the content
function keyDownChange(element1, element2) {
	$(element1).keydown(function() {
		$(element2).css('display', 'none');
	});
}

//Validate the login form
function validateLogin() {
	//Do validation for empty input 
	if (checkEmpty("#userEmail")){
		//Display the error msg
		$(".red").html("Username can't be blank!");
		$(".red").css('display', 'inline');
		
		//Bind the keyUpChange function 
		keyDownChange("#userEmail", ".red");
		
		//Set the focus to the control 
		$("#userEmail").focus();
		
		//break out of the request
		return false;
	} else if (isValidEmailAddress($("#userEmail").val())) {
		return false;
		//Display the error msg
		$(".red").html("Invalid Email Address!");
		$(".red").css('display', 'inline');
		
		//Bind the keyUpChange function 
		keyDownChange("#userEmail", ".red");
		
		//Set the focus to the control 
		$("#userEmail").focus();	
		
		//break out of the request
		return false;					
	} else if(checkEmpty("#userPass")) {
		//Display the error msg
		$(".red").html("Password can't be blank!");
		$(".red").css('display', 'inline');
		
		//Bind the keyUpChange function 
		keyDownChange("#userPass", ".red");
		
		//Set the focus to the control 
		$("#userPass").focus();
		
		//break out of the request
		return false;
	} else {
		$(".red").css('display', 'none');
		$(".red").html('Ivalid Username/Password. <br/>*Notice: Your account will be blocked on the third failed attempt!');
		return true;
	}
}

//Validate the registration form
function validateRegister(form, uid, email, password, conf){
	 // Check each field has a value
	if (uid.value == ''         || 
		  email.value == ''     || 
		  password.value == ''  || 
		  conf.value == '') {
		
		//Display the error msg
		$(".redRegister").html("You must provide all the requested details. Please try again!");
		$(".redRegister").css('display', 'inline');
		
		//Bind the keyUpChange function 
		keyDownChange(form.username, ".redRegister");
		
		//Set the focus to the control 
		form.username.focus();
		
		//break out of the request
		return false;
	}

	// Check the username

	re = /^\w+$/; 
	if(!re.test(form.username.value)) {
		//Display the error msg
		$(".redRegister").html("Username must contain only letters, numbers and underscores. Please try again!");
		$(".redRegister").css('display', 'inline');
		
		//Bind the keyUpChange function 
		keyDownChange(form.username, ".redRegister");		
		//Set focus to the control
		form.username.focus();
		
		//return false
		return false; 
	}

	// Check that the password is sufficiently long (min 6 chars)
	// The check is duplicated below, but this is included to give more
	// specific guidance to the user
	if (password.value.length < 6) {
		
		//Display the error msg
		$(".redRegister").html("Passwords must be at least 6 characters long.  Please try again!");
		$(".redRegister").css('display', 'inline');
		
		//Bind the keyUpChange function 
		keyDownChange(form.password, ".redRegister");		
		//Set focus to the control
		form.username.focus();
		
		//return false
		return false;
	}

	// At least one number, one lowercase and one uppercase letter 
	// At least six characters 

	var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/; 
	if (!re.test(password.value)) {
		//Display the error msg
		$(".redRegister").html("Passwords must contain at least one number, one lowercase and one uppercase letter.  Please try again!");
		$(".redRegister").css('display', 'inline');
		
		//Bind the keyUpChange function 
		keyDownChange(form.password, ".redRegister");		
		//Set focus to the control
		form.username.focus();
		
		//return false
		return false;

	}

	// Check password and confirmation are the same
	if (password.value != conf.value) {
		
		//Display the error msg
		$(".redRegister").html("Your password and confirmation do not match. Please try again!");
		$(".redRegister").css('display', 'inline');
		
		//Bind the keyUpChange function 
		keyDownChange(form.password, ".redRegister");		
		//Set focus to the control
		form.username.focus();
		
		//return false
		return false;		
	}


	//Return success
	return true;
}