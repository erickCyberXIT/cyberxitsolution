	/////////////////////////////////////////////////////
	//
	// Author: Erick Boshoff
	// Date: 2015-16-10
	// Description: The purpose of this document
	// is to make asyncronous calls to the server
	// and loading and appending the content to the dom.
	//
	////////////////////////////////////////////////////
	
	//--> Begin global variables 
			// Variable to hold request
			var request;
	//--> End global variables
	
	//--> Begin main navigation section
	//Home
	function changeHome() {
	        $("#mainContent").load("home.html", function(responseTxt, statusTxt, xhr){
	            if(statusTxt == "success") {			
					$(function() {
			
						$('#da-slider').cslider({
							autoplay	: true,
							bgincrement	: 0
						});
					
					});
					setTimeout(function() {$("#da-slider").fadeTo(1200, 1)}, 200);
		       		setTimeout(function() {$("#footerContent").fadeTo(1200, 1)}, 200);
	       		} else if(statusTxt == "error") {
	                alert("Error: " + xhr.status + ": " + xhr.statusText);
	            }
	        });
	}
	
	//Products
	function changeProducts() {
		//Set main content's opacity to 0
		 $("#mainContent").fadeTo(0, 0);
		 //Initiate page load with ajax jquery
		 $("#mainContent").load("products.html", function(responseTxt, statusTxt, xhr){
            if(statusTxt == "success")
            
            //Style Code here
            setTimeout(function() {$("#mainContent").fadeTo(1200, 1)}, 200);
            setTimeout(function() {$("#footerContent").fadeTo(1200, 1)}, 200);	
            if(statusTxt == "error")
                alert("Error: " + xhr.status + ": " + xhr.statusText);
        });
	}
	
	//Projects
	function changeProjects() {
		//Set main content's opacity to 0
		 $("#mainContent").fadeTo(0, 0);
		 //Initiate page load with ajax jquery
		 $("#mainContent").load("projects.html", function(responseTxt, statusTxt, xhr){
            if(statusTxt == "success")
            
            //Style Code here
            setTimeout(function() {$("#mainContent").fadeTo(1200, 1)}, 200);
            setTimeout(function() {$("#footerContent").fadeTo(1200, 1)}, 200);	
            if(statusTxt == "error")
                alert("Error: " + xhr.status + ": " + xhr.statusText);
        });
	}
	
	//Our story
	function changeStory() {
		//Set main content's opacity to 0
		 $("#mainContent").fadeTo(0, 0);
		 //Initiate page load with ajax jquery
		 $("#mainContent").load("story.html", function(responseTxt, statusTxt, xhr){
            if(statusTxt == "success")
            
            //Style Code here
            setTimeout(function() {$("#mainContent").fadeTo(1200, 1)}, 200);
            setTimeout(function() {$("#footerContent").fadeTo(1200, 1)}, 200);	
            if(statusTxt == "error")
                alert("Error: " + xhr.status + ": " + xhr.statusText);
        });
	}
	var bindContactSub = function() {
		$("#submitQuery").click(function() {	
			//Ajax to send email 
			//validation
			if ($("#emailId").val() == ""){ 
				alert("Please provide a valid email");
				return false;
			}
			if ($("#firstName").val() == "") {
				alert("Please provide a valid firstname");
				return false;
			}
			if ($("#lastName").val() == "") {
				alert("Please provide a valid lastname");
				return false;
			}
			if ($("#tel").val() == "") {
				alert("Please provide a valid contact number");
				return false;
			}
			if ($("#comment").val() == "")  {
				alert("Please provide enquiry details");
				return false;
			}
			
			//Data string
			var dataString = 'email=' + $("#emailId").val() +
							 '&first_name=' + $("#firstName").val() +
							 '&last_name=' + $("#lastName").val() + 
							 '&telephone=' + $("#tel").val() + 
							 '&comments=' + $("#comment").val();
							 
			//Do ajax call to insert client
			$.ajax({
				type: "POST",
				url: "./send_form_email.php",
				data: dataString,
				dataType: 'html',
				success: function(data) {
					//console.log(data); 							
					alert("Your Enquiry has been submitted and our support desk have been notified. We will assist to you in the next 24 hours!");
					
					//clear fields
					$("#emailId").val("");
					$("#firstName").val("");
					$("#lastName").val(""); 
					$("#tel").val(""); 
					$("#comment").val("");
					
					//hit the return click
					$("#btnReturn").click();
				},
				error: function(data) {
					
				}
			});
		});
	};
	
	//Contact us 
	function changeContact() {
		//Set main content's opacity to 0
		 $("#mainContent").fadeTo(0, 0);
		 //Initiate page load with ajax jquery
		 $("#mainContent").load("contact.php", function(responseTxt, statusTxt, xhr){
            if(statusTxt == "success")
            bindContactSub();
            //Style Code here
            setTimeout(function() {$("#mainContent").fadeTo(1200, 1)}, 200);
            setTimeout(function() {$("#footerContent").fadeTo(1200, 1)}, 200);	
            if(statusTxt == "error")
                alert("Error: " + xhr.status + ": " + xhr.statusText);
        });
	}
	var bindSupportSub = function() {
		$("#submitTicket").click(function() {	
			//Ajax to send email 
			//validation
			if ($("#emailId").val() == ""){ 
				alert("Please provide a valid email");
				return false;
			}
			if ($("#firstName").val() == "") {
				alert("Please provide a valid firstname");
				return false;
			}
			if ($("#lastName").val() == "") {
				alert("Please provide a valid lastname");
				return false;
			}
			if ($("#tel").val() == "") {
				alert("Please provide a valid contact number");
				return false;
			}
			if ($("#ticketCategory").val() == "0") {
				alert("Please select a ticket category");
				return false;
			}
			if ($("#ticketLevel").val() == "0") {
				alert("Please select a ticket priority level");
				return false;
			}			
			if ($("#comment").val() == "")  {
				alert("Please provide enquiry details");
				return false;
			}
			
			//Disable button
			$("#submitTicket").prop('disabled', true);
			
			//Data string
			var dataString = 'email=' + $("#emailId").val() +
							 '&first_name=' + $("#firstName").val() +
							 '&last_name=' + $("#lastName").val() + 
							 '&telephone=' + $("#tel").val() + 
							 '&clientCode=' + $("#clientCode").val() + 
							 '&ticketCategory=' + $("#ticketCategory option:selected").text() + 
							 '&ticketLevel=' + $("#ticketLevel option:selected").val() + 
							 '&comments=' + $("#comment").val();
							 
			//Do ajax call to insert client
			$.ajax({
				type: "POST",
				url: "./send_form_email_support.php",
				data: dataString,
				dataType: 'html',
				success: function(data) {
					//console.log(data); 							
					alert("Your Ticket has been logged and our support desk have been notified. We will assist to you in the next 24 hours!");
					
					//clear fields
					$("#emailId").val("");
					$("#firstName").val("");
					$("#lastName").val(""); 
					$("#tel").val(""); 
					$("#clientCode").val("");
					$('select option[value="0"]').attr("selected",true);
					$("#comment").val("");
					//Enable button
					$("#submitTicket").prop('disabled', true);
					
				},
				error: function(data) {
					
				}
			});
		});
	};
	function changeSupport() {
		//Set main content's opacity to 0
		 $("#mainContent").fadeTo(0, 0);
		 //Initiate page load with ajax jquery
		 $("#mainContent").load("support.php", function(responseTxt, statusTxt, xhr){
            if(statusTxt == "success")
			//bind()
			bindSupportSub();
            //Style Code here
            setTimeout(function() {$("#mainContent").fadeTo(1200, 1)}, 200);
            setTimeout(function() {$("#footerContent").fadeTo(1200, 1)}, 200);	
			//animation function rotate
			setInterval(function(){
				var elem = $("#supportImage");

				$({deg: 0}).animate({deg: 360}, {
					duration: 2000,
					step: function(now){
						elem.css({
							 transform: "rotateX(" + now + "deg)"
						});
					}
				});
			}, 10000);
			
							
			 $('html, body').animate({
				scrollTop: parseInt($(".supportImage").offset().top)
			},740);
			
            if(statusTxt == "error")
                alert("Error: " + xhr.status + ": " + xhr.statusText);
        });
	}
	
	// --> End main navigation section
		
		
	// --> Begin login/register calls
		function bindRegister() {
			// Bind to the submit event of our form
			$("#register_form").submit(function(event){	
				
				// Abort any pending request
				if (request) {
					request.abort();
				}
				// setup some local variables
				var $form = $(this);
			
				//Validate the form 
				if(!validateRegister(this, this.username, this.email, this.password, this.confirmpwd)){
					return false;
				}
				
				//Call the hash function from 'clientLoginScript.js'
				regformhash(this, this.username, this.email, this.password, this.confirmpwd);			
											
				// Let's select and cache all the fields
				var $inputs = $form.find("input, select, button, textarea, hidden, text, checked");

				// Serialize the data in the form
				var serializedData = $form.serialize();

				// Let's disable the inputs for the duration of the Ajax request.
				// Note: we disable elements AFTER the form data has been serialized.
				// Disabled form elements will not be serialized.
				$inputs.prop("disabled", true);
			
				// Fire off the request to /form.php
				request = $.ajax({
					url: "clientportal/register.inc.php",
					type: "post",
					data: serializedData
				});

				// Callback handler that will be called on success
				request.done(function (response, textStatus, jqXHR){
					var obj = $.parseJSON(response);
						
					if(obj.status === "success") {
						//Do something with successfull registration
						getRegisterAjax();
					} else {
						//log the error returned from the server.
						//Display the error msg
						$(".redRegister").html(obj.status);
						$(".redRegister").css('display', 'inline');
						
						//Bind the keyUpChange function 
						keyDownChange("#register_form input[name=username]", ".redRegister");		
						//Set focus to the control
						$("#register_form input[name=username]").focus();
						
						//return false
						return false;
					}				
				});

				// Callback handler that will be called on failure
				request.fail(function (jqXHR, textStatus, errorThrown){
					// Log the error to the console
					console.error(
						"The following error occurred: "+
						textStatus, errorThrown
					);
				});

				// Callback handler that will be called regardless
				// if the request failed or succeeded
				request.always(function () {
					// Reenable the inputs
					$inputs.prop("disabled", false);
				});

				// Prevent default posting of form
				event.preventDefault();
			});
		}
		
		
		//Bind toggle 
		function bindTogle(){
			// Expand Panel
			$("#open").on("click", function(){
				$("div#panel").slideDown("slow");
			
			});	
			
			// Collapse Panel
			$("#close").on("click", function(){
				$("div#panel").slideUp("slow");	
			});		
			
			// Switch buttons from "Log In | Register" to "Close Panel" on click
			$("#toggle a").on("click", function () {
				$("#toggle a").toggle();
			});		
		}
				
		function bindReturn() {
			//Return to login area
			$(".bt_return").click(function() {
				//Call Ajax
				renderLogin();			
			});
		}
		
		function bindLoginValidation() {
			//for validation on login
			$(".bt_login").click(function(){
				validateLogin(); //rebind validation		
			});
		}
		
		function bindLogin(){
			// Bind to the submit event of our form
			$("#login_form").submit(function(event){	
				
				// Abort any pending request
				if (request) {
					request.abort();
				}
				// setup some local variables
				var $form = $(this);
						
				//Call the hash function from 'clientLoginScript.js'
				formhash(this, this.password);
				
				// Let's select and cache all the fields
				var $inputs = $form.find("input, select, button, textarea, hidden, text, checked");

				// Serialize the data in the form
				var serializedData = $form.serialize();

				// Let's disable the inputs for the duration of the Ajax request.
				// Note: we disable elements AFTER the form data has been serialized.
				// Disabled form elements will not be serialized.
				$inputs.prop("disabled", true);
			
				// Fire off the request to /form.php
				request = $.ajax({
					url: "clientportal/process_login.php",
					type: "post",
					data: serializedData
				});

				// Callback handler that will be called on success
				request.done(function (response, textStatus, jqXHR){
					var obj = $.parseJSON(response);
						
					if(obj.status ==="success") {
						//Call the getLoinAjax function 
						getLoginAjax(obj.companyName, obj.companyInfo);	
					}
					
					if (obj.status === "blocked") {
						$(".red").html("Account blocked, please check your email for instructions to unblock account.");
						$(".red").css('display', 'inline');
					} else if (obj.status === "failed") {
						$(".red").html("Ivalid Username/Password. <br/>*Notice: Your account will be blocked on the third failed attempt!s");
						$(".red").css('display', 'inline');
					}
				});

				// Callback handler that will be called on failure
				request.fail(function (jqXHR, textStatus, errorThrown){
					// Log the error to the console
					console.error(
						"The following error occurred: "+
						textStatus, errorThrown
					);
				});

				// Callback handler that will be called regardless
				// if the request failed or succeeded
				request.always(function () {
					// Reenable the inputs
					$inputs.prop("disabled", false);
				});

				// Prevent default posting of form
				event.preventDefault();
			});
		}
		
		function bindLogout(){
			$("#bt_logout").click(function(){
				
				// Abort any pending request
				if (request) {
					request.abort();
				}
				
				// Fire off the request to /logout.php
				request = $.ajax({
					url: "clientportal/logout.php",
					type: "post"
				});

				// Callback handler that will be called on success
				request.done(function (response, textStatus, jqXHR){
					var obj = $.parseJSON(response);
						
					if(obj.status ==="success") {
						//Call the getLoinAjax function 
						getLoginAjax();
					}
				});

				// Callback handler that will be called on failure
				request.fail(function (jqXHR, textStatus, errorThrown){
					// Log the error to the console
					console.error(
						"The following error occurred: "+
						textStatus, errorThrown
					);
				});
			});	
		}
					
		function bindControls() {
			
			bindLoginValidation(); //Rebind login validation
		
			bindLogin(); //Rebind form	
		
			bindRegister(); //Rebind register success			
				
			bindLogout(); //logout				
			
			bindReturn();
		}
					
		function getRegisterAjax(){
			//Fade  Out
			$(".left").fadeOut(600);
			
			setTimeout(function(){
				//Initiate page load with ajax jquery
				$("#toppanel").load("./register_pages/register_success.php", function(responseTxt, statusTxt, xhr){
					if(statusTxt == "success"){						
						//bind controls
						bindReturn(); //return to login
						bindTogle();  //Panel
					}
					if(statusTxt == "error") {
						alert("Error: " + xhr.status + ": " + xhr.statusText);
					}
				});
			}, 600);
			
			//Fade  In
			$(".left").fadeIn(600);		
		}
		
		//Ajax function to append the content
		function getLoginAjax(companyName, companyInfo){
			//Fade  Out
			$(".left").fadeOut(600);
			$(".login").fadeOut(600);
			
			setTimeout(function(){
				//Initiate page load with ajax jquery
				$("#toppanel").load("./login_pages/login_success.php", function(responseTxt, statusTxt, xhr){
					if(statusTxt == "success"){	
						bindControls();
						bindTogle();  //Panel	
						//set the inner html of the client 
						$("#companyName").html(companyName);
						$("#companyInfo").html(companyInfo);
						
					}
					if(statusTxt == "error") {
						alert("Error: " + xhr.status + ": " + xhr.statusText);
					}
				});
			}, 600);
			
			//Fade  In
			$(".left").fadeIn(600);
			$(".login").fadeIn(600);
		}
		
		//only render login
		function renderLogin(){
			//Fade  Out
			$(".left").fadeOut(600);
			
			setTimeout(function(){
					//Initiate page load with ajax jquery
					$("#toppanel").load("./login_pages/login_success.php", function(responseTxt, statusTxt, xhr){
						if(statusTxt == "success"){		
							bindControls();	
							bindTogle();  //Panel							
						}
						if(statusTxt == "error") {
							alert("Error: " + xhr.status + ": " + xhr.statusText);
						}
					});
			}, 600);
			
			//Fade  In
			$(".left").fadeIn(600);
			$(".login").fadeIn(600);
		}
		
		
		//Init bind
		bindControls();
					
	// --> End login/logout  calls 
	
	//log query back 
	$("#thankyouBack").click(function(){
		changeContact();
	});
		