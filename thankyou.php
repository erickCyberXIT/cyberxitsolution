<html>
    <head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  
		<!-- stylesheets -->
	  	<link rel="stylesheet" href="styles/style.css" type="text/css" media="screen" />
	  	<link rel="stylesheet" href="styles/slide.css" type="text/css" media="screen" />
       	<link rel="stylesheet" type="text/css" href="styles/style2.css" />
       	
		<!--navigation menue-->
		<link rel="stylesheet" href="styles/nav.css">
			
       	<!-- jQuery - the core -->
		<!-- Sliding effect -->
		<script type="text/javascript" src="Scripts/modernizr.custom.28468.js"></script>
		<link href='http://fonts.googleapis.com/css?family=Economica:700,400italic' rel='stylesheet' type='text/css'>
		<noscript>
		<link rel="stylesheet" type="text/css" href="styles/nojs.css" />
		</noscript>
        <link rel="stylesheet" type="text/css" href="styles/cyberxCSS.css">
       
        <!-- Pushy CSS -->
        <link rel="stylesheet" href="styles/normalize.css">
        <link rel="stylesheet" href="styles/demo.css">
        <link rel="stylesheet" href="styles/pushy.css">
		<script src="Scripts/jquery-1.12.0.min.js"></script>
		<script src="Scripts/jssor.slider.mini.js"></script>
		<script src="Scripts/validationScript.js"></script>
		<script type="text/JavaScript" src="Scripts/sha512.js"></script> 
		<script src="Scripts/slide.js" type="text/javascript"></script>
        <title>
    		CyberX IT 
    	</title>
	   <script type="text/javascript"> 
		 var is_mobile = !!navigator.userAgent.match(/iphone|android|blackberry/ig) || false;
		 
		 if(is_mobile){
			window.location = "http://www.mobi.cyberxit.co.za";
		 }
	   </script>
    </head>
    <body>
	  	 <!-- Pushy Menu -->
        <nav class="pushy pushy-left">
            <ul>
                <li class="pushy-link">
                    <a href="#" onclick="javascript:changeHome()">Home</a>
                </li>
                <li class="pushy-submenu">
                    <a href="#">💡 SOLUTIONS</a>
                    <ul>
                        <li class="pushy-link"><a href="#" onclick="javascript:changeProducts()">► Web Solutions</a></li>
                        <li class="pushy-link"><a href="#" onclick="javascript:changeProducts()">► Hosting Solutions</a></li>
                        <li class="pushy-link"><a href="#" onclick="javascript:changeProducts()">► Software Solutions</a></li>
						<li class="pushy-link"><a href="#" onclick="javascript:changeProducts()">► Maintenance</a></li>
                    </ul>
                </li>
                <li class="pushy-submenu">
                    <a href="#">► Specials</a>
                    <ul>
                        <li class="pushy-link"><a href="#">No Specials Currently.</a></li>
                    </ul>
                </li>
                <li class="pushy-link">
                    <a href="#" onclick="javascript:changeProjects()">☑ PROJECTS</a>
                </li>
				<li class="pushy-link">
					<a href="#" onclick="javascript:changeStory()">📖 OUR STORY</a>
				</li>
				<li class="pushy-link">
					<a href="#" onclick="javascript:changeContact()">☎ CONTACT US</a>
				</li>				
            </ul>
        </nav>

        <!-- Site Overlay -->
        <div class="site-overlay"></div>
		
		<div id="wrapper">
			<div id="container">
				<a href="http://www.cyberxit.co.za"><img width="470px" style="position: absolute; left: 0px; right: 0px; margin: 0px auto; opacity: 1; z-index: 999; "src="Images/cyberLogo.gif" id="logoImage"  /></a>
				<div class="menu-btn">&#9776; Menu</div>
				<div id="mainContent">
					<div id="contact">
						<div class="headerBackground">
						   <h1>CONTACT US</h1>
						</div>
						<div id="contactContainer">
							<table id="contactTable" style="border: none; display: initial; left: 9%;" cellpadding="10">
								<tr>
								 
								 <td valign="top">
									Thank you for your enquiry – we will be in touch within the next 24 hours!
								 </td>
								 
								
								</tr>
								<tr>
									<br/>
								</tr>
							</table>
							<input type="button" class="buttonStyle" id="thankyouBack" value="back" style="position: relative; left: -13%; top: 90px;  width:205px" />
						</div>
					</div>
				   <!--This is the page footer-->
					<div id="footerContent" style="opacity: 0.6;">
						<table>
							<tr>
								<td  style="position: relative; "><span>&#9786;</span>&nbsp;Support</td>
								<td  style="position: relative; "><span>&#x260E;</span>&nbsp;Contact Details</td>
								<td  style="padding-right: 20px; position: relative;"><a href="http://www.management.cyberxit.co.za/">GO TO MY WEBSITE</a></td>		
								<td  style="position: relative; "><span>&#9990;</span>&nbsp;Social Media</td>
								<td  style="position: relative; "><span>&#x3f;</span>&nbsp;About</td>
							</tr>
							<tr>
								<td>
									<ul style="font-size: 11pt; list-style-type: none;">
										<li>e-mail: support@cyberxit.co.za</li>
										<li>Log a ticket: <a href="http://www.management.cyberxit.co.za/">www.management.cyberxit.co.za</a></li>
									</ul>
								</td>	
								<td>
									<ul style="font-size: 11pt; list-style-type: none;">
										<li>e-mail: sales@cyberxit.co.za</li>
										<li>Address: 31st Avenue Gauteng Pretoria</li>
									</ul>
								</td>
								<td>
									
								</td>
								<td>
									 <ul style="font-size: 11pt; list-style-type: none;">
										<li><a href="https://www.facebook.com/cyberxit?ref=ts&fref=ts"><img id="facebook" src="Images/facebook.png" width="50" height="50"/></a></li>
									</ul>
								</td>	
								<td>
									 <ul style="font-size: 11pt; list-style-type: none;">
										<li>
											We CyberX IT specialize in the latest web software<br/>
											to suit your needs.
										</li>
									 </ul>
								</td>	
							</tr>
						</table>
						<div>Copyright © 2014. CyberX IT all rights reserved</div>
					</div>
				</div>
		   	</div>
		</div>
    </body>
	<script src="Scripts/pushy.min.js"></script>
	<script type="text/javascript" src="Scripts/ajaxScript.js"></script>
	<script src="Scripts/clientLoginScript.js"></script>
	<script type="text/javascript" src="Scripts/jquery.cslider.js"></script>
	<!-- Pushy JS -->
	<script>
	//Do ajax call to insert client
	$.ajax({
		type: "POST",
		url: "./Database/client_functions.php",
		data: dataString,
		dataType: 'html',
		success: function(data) {
			//console.log(data); (Test)
			
			if (data == "failed_name" || data == "failed_code" || data == "failed_email" || data == "failed_contact") {
				alert("Please fix validation errors");
				return;
			} else if ($("#clientAddClientBtnCreate").val().toLowerCase() == "update") {
					$("#dialog-clients").attr("title", "Add Client");
					$("#clientAddClientBtnCreate").val("Add");
					$("#add_client").text("Add Client");
					$("#clientAddClientBtn").val("Add Client");
					$(".visiblity").fadeOut(600);		
					$("#clientNameText").val("");  
					$("#clientCodeText").val(""); 
					$("#clientEmailText").val(""); 
					$("#clientContactText").val("");
					$("#clientAddressText").val(""); 
					$("#clientInfoText").val(""); 
					alert("Client Successfully Updated!");
					$( "#dialog-clients" ).dialog("close");
					$("#clientSearchBtn").click(); //trigger to refresh list
			} else if (confirm("Client Successfully Added! Add another?")) {
					$("#clientNameText").val("");  
					$("#clientCodeText").val(""); 
					$("#clientEmailText").val(""); 
					$("#clientContactText").val("");
					$("#clientAddressText").val(""); 
					$("#clientInfoText").val(""); 
			} else {
				$( "#dialog-clients" ).dialog("close");
				$("#clientAddClientBtn").val("Add Client");
				$(".visiblity").fadeOut(600);		
				$("#clientSearchBtn").click(); //refresh grid
			}															
		},
		error: function(data) {
			
		}
	});
	</script>
	
</html>
