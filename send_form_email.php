<?php
 
if(isset($_POST['email'])) {
 
	$first_name = $_POST['first_name']; // required
 
    $last_name = $_POST['last_name']; // required
 
    $email_from = $_POST['email']; // required
 
    $telephone = $_POST['telephone']; // not required
 
    $comments = $_POST['comments']; // required
 
	//validate 
	if ($first_name == "") {
		echo "firstname required";
		return false;
	} else if ($last_name == "") {
		echo "lastname required";
		return false;
	} else if ($email_from == "") {
		echo "email required";
		return false;
	} else if ($telephone == "") {
		echo "telephone required";
		return false;
	} 
 
	//Email Details
    $email_message = "Form details below.\n\n";
     
    $email_message .= "First Name: " .$first_name."\n"; 
 
    $email_message .= "Email: ".$email_from."\n";
 
    $email_message .= "Telephone: ".$telephone."\n";
 
    $email_message .= "Client Enquiry Details: ".$comments."\n";
  
	//Send Email
    $email_to = "support@cyberxit.co.za" . "\n";
	
    $email_subject = "Client Query From: " . $first_name . " " . $last_name;
	
	$message = "<html>
					<head>
						<title>CyberX IT Query</title>
					</head>
					<style>
						table {
							border-collapse: collapse;
							width: 100%;
						}

						th, td {
							text-align: left;
							padding: 8px;
						}

						tr:nth-child(even){background-color: #f2f2f2}

						th {
							background-color: #818181;
							color: white;
						}
					</style>
					<body>
						
						<table style=\"border: 1px solid black;\" >
							<th colspan=\"3\">Query Information</th>
							<tr>
								<td>
									<span>Firstname:</span>
								<td>
								<td>
									<span>" . $first_name . "</span>
								</td>
							</tr>
							<tr>
								<td>
									<span>Lastname:</span>
								<td>
								<td>
									<span>" . $last_name. "</span>
								</td>
							</tr>
							<tr>
								<td>
									<span>Email:</span>
								<td>
								<td>
									<span>" . $email_from . "</span>
								</td>
							</tr>
							<tr>
								<td>
									<span>Telephone:</span>
								<td>
								<td>
									<span>" . $telephone . "</span>
								</td>
							</tr>
							<tr>
								<td>
									<span>Message:</span>
								<td>
								<td>
									<span>" . $comments. "</span>
								</td>
							</tr>
						</table>
						<br />
						
						<h2>This is an automated query sent by the CyberX IT email system.</h2>
						<p>Please see that this query is resolved in the next 24 hours.</p>
					
						
						<img src=\"http://cyberxit.co.za/Images/cyber.jpg\" width=\"150px\" height=\"80px\" />
					</body>
				</html>";
				
		//Set headers
		$headers = "MIME-Version: 1.0" . "\r\n"; //Version
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; //Set content type for html mail
		$headers .= "From: <" . $email_from . ">" . "\r\n"; //Requested user
		$headers .= "Cc: johan@cyberxit.co.za" . "\r\n"; //Send to Johan as well
		
		@mail($email_to, $email_subject, $message, $headers);
	
	//Return success
	echo "success";

?>
  
<?php
 
}
 
?>