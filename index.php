<?php
include_once 'clientportal/db_connect.php';
include_once 'clientportal/functions.php';
 
sec_session_start();
 
if (login_check($mysqli) == true) {
    $logged = 'in';
} else {
    $logged = 'out';
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
    <head>        
	    <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  
		<!-- stylesheets -->
	  	<link rel="stylesheet" href="styles/style.css" type="text/css" media="screen" />
	  	<link rel="stylesheet" href="styles/slide.css" type="text/css" media="screen" />
       	<link rel="stylesheet" type="text/css" href="styles/style2.css" />
       	
		<!--navigation menue-->
		<link rel="stylesheet" href="styles/nav.css">
			
       	<!-- jQuery - the core -->
		<!-- Sliding effect -->
		<script type="text/javascript" src="Scripts/modernizr.custom.28468.js"></script>
		<link href='http://fonts.googleapis.com/css?family=Economica:700,400italic' rel='stylesheet' type='text/css'>
		<noscript>
		<link rel="stylesheet" type="text/css" href="styles/nojs.css" />
		</noscript>
        <link rel="stylesheet" type="text/css" href="styles/cyberxCSS.css">
       
        <!-- Pushy CSS -->
        <link rel="stylesheet" href="styles/normalize.css">
        <link rel="stylesheet" href="styles/demo.css">
        <link rel="stylesheet" href="styles/pushy.css">
		<script src="Scripts/jquery-1.12.0.min.js"></script>
		<script src="Scripts/jssor.slider.mini.js"></script>
		<script src="Scripts/validationScript.js"></script>
		<script type="text/JavaScript" src="Scripts/sha512.js"></script> 
		<script src="Scripts/slide.js" type="text/javascript"></script>
        <title>
    		CyberX IT 
    	</title>
   <script type="text/javascript"> 
  	 var is_mobile = !!navigator.userAgent.match(/iphone|android|blackberry/ig) || false;
  	 
  	 if(is_mobile){
	 	window.location = "http://www.mobi.cyberxit.co.za";
  	 }
   </script>
    </head>
    <body>
       	 <!-- Pushy Menu -->
        <nav class="pushy pushy-left">
            <ul>
                <li class="pushy-link">
                    <a href="#" onclick="javascript:changeHome()">Home</a>
                </li>
                <li class="pushy-submenu">
                    <a href="#">💡 Solutions</a>
                    <ul>
                        <li class="pushy-link"><a href="#" onclick="javascript:changeProducts()">► Web Solutions</a></li>
                        <li class="pushy-link"><a href="#" onclick="javascript:changeProducts()">► Hosting Solutions</a></li>
                        <li class="pushy-link"><a href="#" onclick="javascript:changeProducts()">► Software Solutions</a></li>
						<li class="pushy-link"><a href="#" onclick="javascript:changeProducts()">► Maintenance</a></li>
                    </ul>
                </li>
                <li class="pushy-submenu">
                    <a href="#">► Specials</a>
                    <ul>
                        <li class="pushy-link"><a href="#">No Specials Currently.</a></li>
                    </ul>
                </li>
                <li class="pushy-link">
                    <a href="#" onclick="javascript:changeProjects()">☑ Projects</a>
                </li>
				<li class="pushy-link">
					<a href="#" onclick="javascript:changeStory()">📖 Our Story</a>
				</li>
				<li class="pushy-link">
					<a href="#" onclick="javascript:changeContact()">☎ Contact Us</a>
				</li>				
				<li class="pushy-link">
					<a href="#" onclick="javascript:changeSupport()">&#x271A; Support Desk</a>
				</li>
            </ul>
        </nav>

        <!-- Site Overlay -->
        <div class="site-overlay"></div>
<!-- Panel -->
<div id="toppanel">
<?php
	echo "<div id=\"panel\">
		<div  class=\"content clearfix\" style=\"margin:0px auto; border:none; position: relative;\">";
			
						if (isset($_GET['error'])) {
							echo '<p class="error">Error Logging In!</p>';
						}
						if (login_check($mysqli) == true) {
							//Create object to hold result
							$obj = new stdClass();
							$obj = getUserDetails(htmlentities($_SESSION['email_ses']), $mysqli); 
										
							echo "<div class=\"left\">
									<h2 id=\"companyName\">";
										echo $obj->companyName;
										echo "</h2>		
									<p id=\"companyInfo\" class=\"grey\">"; echo $obj->companyInfo; 
									echo "</p>	
								</div>";
							echo "<div class=\"left\" id=\"mainLogin\">
									<h2>Welcome ". htmlentities($_SESSION['username']) . "</h2>";
								echo '<p>Currently logged ' . $logged . ' as ' . htmlentities($_SESSION['username']) . '.</p>';
								echo '<p>Do you want to change user or logout? <a id="bt_logout" href="#">Log out</a>.</p>';
							echo "</div>";
						} else {						
							echo "<div class=\"left\">
				
								<h2>Client Login Portal</h2>		
								<p class=\"grey\">All registered CyberX IT clients can use this login form to login to their online client portal, this will allow you to
								access all your project content and client details. If you are not a client at CyberX IT you can still register to get all the latest
								updates on all new software release!</p>
								
							</div>
							<div class=\"left\" id=\"mainLogin\">
									<form id=\"login_form\" class=\"clearfix-login\">        
										<h2>Client Login</h2>
										<label class=\"grey\" for=\"name\">Username:</label>
										<input id=\"userEmail\" class=\"field\" type=\"text\" name=\"email\" />
										<label class=\"grey\" for=\"pass\">Password:</label>
										<input id=\"userPass\" class=\"field\" type=\"password\" name=\"password\" id=\"password\"/>
										<label class=\"red\" style=\"display: none;\">Ivalid Username/Password. <br/>*Notice: Your account will be blocked on the third failed attempt!</label>
										<label><input name=\"rememberme\" id=\"rememberme\" type=\"checkbox\" checked=\"checked\" value=\"forever\" /> &nbsp;Remember me</label>
										<div class=\"clear\"></div>							
										<a class=\"lost-pwd\" href=\"#\">Lost your password?</a>						
									
										<input class=\"bt_login\" type=\"submit\" 
											   value=\"Login\" /> 
									</form>
						
							</div>";
							echo "<div id=\"mainRegister\" class=\"left right\"> 	
									<form id=\"register_form\"> 
										<h2>Client Registration</h2>						
										<label class=\"grey\" for=\"username\">Username:</label>
										<input class=\"field\" type='text' name='username' id='username' />
										<label class=\"grey\" for=\"email\">Email:</label> 
										<input class=\"field\" type=\"text\" name=\"email\" id=\"email\" />
										<label class=\"grey\" for=\"password\">Password:</label> 
										<input class=\"field\" type=\"password\" name=\"password\" id=\"password\"/>
										<label class=\"grey\" for=\"confirmpwd\">Confirm Password:</label> 
										<input class=\"field\" type=\"password\" name=\"confirmpwd\" id=\"confirmpwd\" />
										<label class=\"redRegister\" style=\"display: none;\"></label>
										<input class=\"bt_register\" type=\"submit\" value=\"Register\" /> 
									</form>
								</div>";
						}
		
			
		echo "</div>
		</div>";


	echo "<div class=\"tab\"><ul class=\"login\">
				<li class=\"left\">&nbsp;</li>
				<li>";
					if(login_check($mysqli) == true) {
						echo htmlentities($_SESSION['username']);
					} else {
						echo "Guest User"; 
					}
				echo "</li>
					<li class=\"sep\">|</li>
					<li id=\"toggle\">
						<a id=\"open\" class=\"open\" href=\"#\">";
					if(login_check($mysqli) == true) {
						echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Logout";
					} else { 
						echo "Login | Register";
					} 
				echo "</a>
					  <a id=\"close\" style=\"display: none;\" class=\"close\" href=\"#\">Close Panel</a>			
					</li>
					<li class=\"right\">&nbsp;</li>
			</ul></div>";
?>  
</div> 
		<div id="wrapper">
			<div id="container">
			<a href="http://www.cyberxit.co.za"><img src="Images/cyberLogo.gif" id="logoImage"  /></a>
			<div class="menu-btn">&#9776; Menu</div>
	    	<!--This is the main content-->
	    	<div id="mainContent">
					<div id="da-slider" class="da-slider">
						<div class="da-slide">
							<h2>Easy Access</h2>
							<p>Software that allows you to access your data on the fly, we Cyber X IT give you compatible and reliable software for any device!</p>
							
							<div style="width: 0px; height: 0px;" class="da-img"><img src="Images/2.png" width="500" height="400" alt="image01" /></div>
						</div>
						<div class="da-slide">
							<h2>Providing the Best Service</h2>
					
							<p>We at Cyber X IT deliver the best quality products to our clients, with up to standard and fully quality controlled software Cyber X IT provides you with the best client satisfaction.</p>
							
							<div class="da-img"><img src="Images/3.png" alt="image01" width="300" height="300" /></div>
						</div>
						<div class="da-slide">
							<h2>Best Cloud Solutions</h2>
			
							<p>Giving your business the best cloud support to keep your data as safe as possible.</p>
							
							<div class="da-img"><img src="Images/1.png" alt="image01" width="500" height="400"/></div>
						</div>
						<div class="da-slide">
							<h2>Global Computing</h2>
						
							<p>Take your company global with Cyber X IT''s global support we will provide you with the best solutions to suit your needs.</p>
							
							<div class="da-img"><img src="Images/4.png" alt="image01" width="500" height="400" /></div>
						</div>
						<nav class="da-arrows">
							<span class="da-arrows-prev" style="opacity: 1;"></span>
							<span class="da-arrows-next" style="opacity: 1;"></span>
						</nav>
					</div>			
        			   <!--This is the page footer-->
			    		<div id="footerContent">
				    		<table>
				    			<tr>
				    				<td  style="position: relative; "><span>&#x1f527;</span>&nbsp;Support</td>
				    				<td  style="position: relative; "><span>&#x260E;</span>&nbsp;Contact Details</td>
				    				<td  style="padding-right: 20px; position: relative;"><a href="http://www.management.cyberxit.co.za/">GO TO MY WEBSITE</a></td>		
				    				<td  style="position: relative; "><span>&#9990;</span>&nbsp;Social Media</td>
				    				<td  style="position: relative; "><span>&#x3f;</span>&nbsp;About</td>
				    			</tr>
				    			<tr>
				    				<td>
				    					<ul style="font-size: 11pt; list-style-type: none;">
				    						<li>e-mail: support@cyberxit.co.za</li>
				    						<li>Log a ticket: <a href="http://www.management.cyberxit.co.za/">www.management.cyberxit.co.za</a></li>
				    					</ul>
				    				</td>	
				    				<td>
				    					<ul style="font-size: 11pt; list-style-type: none;">
				    						<li>e-mail: sales@cyberxit.co.za</li>
				    						<li>Address: 31st Avenue Gauteng Pretoria</li>
				    					</ul>
				    				</td>
				    				<td>
				    					
				    				</td>
				    				<td>
				    					 <ul style="font-size: 11pt; list-style-type: none;">
				    						<li><a href="https://www.facebook.com/cyberxit?ref=ts&fref=ts"><img id="facebook" src="Images/facebook.png" width="50" height="50"/></a></li>
				    					</ul>
				    				</td>	
				    				<td>
				    					 <ul style="font-size: 11pt; list-style-type: none;">
				    					 	<li>
				    					 		We CyberX IT specialize in the latest web software<br/>
				    					 		to suit your needs.
				    					 	</li>
				    					 </ul>
				    				</td>	
				    			</tr>
				    		</table>
					<div>Copyright © 2014. CyberX IT all rights reserved</div>
					</div>
			</div>
		</div>
		</div>
    </body>
	<script type="text/javascript" src="Scripts/ajaxScript.js"></script>
	<script type="text/javascript" src="Scripts/cyberxScript.js"></script>
	<script src="Scripts/clientLoginScript.js"></script>
	<script type="text/javascript" src="Scripts/jquery.cslider.js"></script>
	<!-- Pushy JS -->
    <script src="Scripts/pushy.min.js"></script>

	<script type="text/javascript">
		$(function() {
			
			$('#da-slider').cslider({
				autoplay	: true,
				bgincrement	: 0
			});
		
		});
	</script>	
</html>