<!--This is the main content-->
<div class="supportImage">
	<img id="supportImage" src="Images/cybersupport.png" width="340px" height="170px" />
</div>
<div id="support">
	<div id="supportContainer">
		<table id="supportTicketTable"  style="border-collapse: initial;"cellpadding="10">
			<tr>
				<th colspan="3" style="text-align: center; font-size: 22px;">
					Log a ticket:		
				</th>
			</tr>
			<tr>
			 
			 <td valign="top">
			 
			  <label for="first_name">Firstname:</label>
			 
			 </td>
			 
			 <td valign="top">
			 
			  <input id="firstName" type="text" name="first_name" maxlength="50" size="30" required />
			 
			 </td>
			 
			</tr>
			 
			<tr>
			 
			 <td valign="top"">
			 
			  <label for="last_name">Lastname:</label>
			 
			 </td>
			 
			 <td valign="top">
			 
			  <input id="lastName" type="text" name="last_name" maxlength="50" size="30" required />
			 
			 </td>
			 
			</tr>
			 
			<tr>
			 
			 <td valign="top">
			 
			  <label for="email">Email Address:</label >
			 
			 </td>
			 
			 <td valign="top">
			 
			  <input id="emailId" type="text" name="email" maxlength="80" size="30" required />
			 
			 </td>
			 
			</tr>
			 
			<tr>
				<td valign="top">
					<label for="telephone">Contact Number:</label>
				</td>
				<td valign="top">
					<input id="tel" type="text" name="telephone" maxlength="30" size="30" />
				</td>
			</tr>
			<tr>
				<td valign="top">
					<label for="clientCode">Client Code (If applicable):</label>
				</td>
				<td valign="top">
					<input id="clientCode" type="text" name="clientCode" maxlength="30" size="30" />
				</td>
			</tr>			
			<tr>
				<td valign="top">
					<label for="ticketCategory">Ticket Category:</label>
				</td>
				<td valign="top">
					<select id="ticketCategory" style="width: 279px;">
						<option value="0">Please select category</option>
						<option value="1">Web Software</option>
						<option value="2">Desktop Software</option>
						<option value="3">Hosting</option>
						<option value="4">Emails</option>
						<option value="5">Maintenance</option>
					</select>
				</td>
			</tr>
			
			<tr>
				<td valign="top">
					<label for="ticketLevel">Ticket Priority Level:</label>
				</td>
				<td valign="top">
					<select id="ticketLevel" style="width: 279px;">
						<option value="0">Please select level</option>
						<option value="1">Low</option>
						<option value="2">Medium</option>
						<option value="3">High</option>
						<option value="4">Critical</option>
					</select>
				</td>
			</tr>
			
			<tr>
			 <td valign="top">
			  <label for="comments">Additional Info:</label>
			 </td>
			 <td valign="top">
			  <textarea id="comment" name="comments" maxlength="1000" cols="32" rows="6" style=" resize: none;" required></textarea>
			 </td>
			</tr>
			 
			<tr>
			 <td>
			 	
			 </td>
			 <td style="text-align:left">
			 
			  <input id="submitTicket" type="submit" class="buttonStyle" style="background-color: #FD6B6B; width: 160px" value="Submit Ticket">
			  <br />
			  <br />
			  <br />
			 </td>
			 
			</tr>
		</table>
	</div>
	<div id="footerContent" style="background: url('Images/footersupport.png'); position: relative; top: 210px;">
				    	<table>
				    			<tr>
				    				<td  style="position: relative; "><span>&#9786;</span>&nbsp;Support</td>
				    				<td  style="position: relative; "><span>&#x260E;</span>&nbsp;Contact Details</td>
				    				<td  style="padding-right: 20px; position: relative;"><a href="http://www.management.cyberxit.co.za/">GO TO MY WEBSITE</a></td>		
				    				<td  style="position: relative; "><span>&#9990;</span>&nbsp;Social Media</td>
				    				<td  style="position: relative; "><span>&#x3f;</span>&nbsp;About</td>
				    			</tr>
				    			<tr>
				    				<td>
				    					<ul style="font-size: 11pt; list-style-type: none;">
				    						<li>e-mail: support@cyberxit.co.za</li>
				    						<li>Log a ticket: <a href="http://www.management.cyberxit.co.za/">www.management.cyberxit.co.za</a></li>
				    					</ul>
				    				</td>	
				    				<td>
				    					<ul style="font-size: 11pt; list-style-type: none;">
				    						<li>e-mail: sales@cyberxit.co.za</li>
				    						<li>Address: 31st Avenue Gauteng Pretoria</li>
				    					</ul>
				    				</td>
				    				<td>
				    					
				    				</td>
				    				<td>
				    					 <ul style="font-size: 11pt; list-style-type: none;">
				    						<li><a href="https://www.facebook.com/cyberxit?ref=ts&fref=ts"><img id="facebook" src="Images/facebook.png" width="50" height="50"/></a></li>
				    					</ul>
				    				</td>	
				    				<td>
				    					 <ul style="font-size: 11pt; list-style-type: none;">
				    					 	<li>
				    					 		We CyberX IT specialize in the latest web software<br/>
				    					 		to suit your needs.
				    					 	</li>
				    					 </ul>
				    				</td>	
				    			</tr>
				    		</table>
				<div>Copyright © 2014. CyberX IT all rights reserved</div>
	 	</div>
	</div>
</div>