//Redirect constructor
function goto(url) {
	window.location.href = url;
}

//REDIRECT FUNCTIONS
$("#home").click( function() {  //For the solutions
	goto("index.html");	
});

$("#solutions").click( function() {  //For the solutions
	goto("solutions.html");	
});

$("#specials").click( function() {  //For the solutions
	goto("specials.html");	
});

$("#aboutUs").click( function() {  //For the solutions
	goto("aboutUs.html");	
});

$("#contactUs").click( function() {  //For the solutions
	goto("contactUs.html");	
});


//SCROLL FUNCTIONS
$("#up-arrow").click(function() {
	$('html, body').animate({
		scrollTop: $("body").offset().top
	}, 500);
});

$("#down-arrow").click(function() {
	$('html, body').animate({
		scrollTop:  $(document).height() - $(window).height() 
	}, 500);
});


var container = $(".solutions_sub")
    
/////////////////WEB SOLUTIONS////////////////////////
$(".webSolutions").click(function() { 
	var scrollTo = $(".webSolutions");
	if ($(".webSolutionContent").is(':visible')) {
		$(".webSolutionContent").slideUp(700);
	} else {
		$(".webSolutionContent").slideDown(700);
	}
});
///////////////HOST SOLUTIONS////////////////////////
$(".hostSolutions").click(function() { 
	if ($(".hostSolutionContent").is(':visible')) {
		$(".hostSolutionContent").slideUp(700);
	} else {
		$(".hostSolutionContent").slideDown(700);
	}
});
//////////////SOFTWARE SOLUTIONS/////////////////////
$(".softSolutions").click(function() { 
	if ($(".softSolutionContent").is(':visible')) {
		$(".softSolutionContent").slideUp(700);
	} else {
		$(".softSolutionContent").slideDown(700);
	}
});
//////////////MAINTENANCE///////////////////////////
$(".maint").click(function() { 
	if ($(".maintContent").is(':visible')) {
		$(".maintContent").slideUp(700);
	} else {
		$(".maintContent").slideDown(700);
	}
});